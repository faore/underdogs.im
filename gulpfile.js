var gulp   = require('gulp'),
concat     = require('gulp-concat'),
uglify     = require('gulp-uglify'),
imagemin   = require('gulp-imagemin'),
sourcemaps = require('gulp-sourcemaps'),
sass       = require('gulp-ruby-sass'),
phpunit    = require('gulp-phpunit'),
plumber    = require('gulp-plumber'),
newer      = require('gulp-newer'),
del        = require('del');

// TODO: Livereload

var paths = {
	scripts: ['app/javascript/vendor/jquery.js', 'app/javascript/vendor/jquery.form.min.js' ,'app/javascript/vendor/uikit.js' ,'app/javascript/vendor/jquery.cycle2.js' ,'app/javascript/vendor/codemirror.js' ,'app/javascript/vendor/markdown.js' ,'app/javascript/vendor/overlay.js' ,'app/javascript/vendor/xml.js' ,'app/javascript/vendor/gfm.js' ,'app/javascript/vendor/marked.js' ,'app/javascript/vendor/markdownarea.js' ,'app/javascript/vendor/form-file.js' ,'app/javascript/vendor/form-password.js' ,'app/javascript/vendor/notify.js' ,'app/javascript/vendor/overlay.js' ,'app/javascript/vendor/sortable.js' ,'app/javascript/vendor/sticky.js' ,'app/javascript/vendor/timepicker.js', 'app/javascript/vendor/jquery.colorbox.js', 'app/javascript/application.js'],
	sass: 'app/sass/*.scss',
	images: 'app/images/**/*',
	tests: 'app/**/*.php'
};

// Not all tasks need to use streams
// A gulpfile is just another node program and you can use all packages available on npm
gulp.task('clean', function(cb) {
	// You can use multiple globbing patterns as you would with `gulp.src`
	del(['public/css', 'public/img', 'public/js'], cb);
});

gulp.task('sass', function() {
	gulp.src(paths.sass)
		.pipe(newer('public/css'))
		.pipe(sass({style: 'compressed'}))
		.pipe(gulp.dest('public/css'));
});

gulp.task('scripts', function() {
	// Minify and copy all JavaScript
	// with sourcemaps all the way down
	gulp.src(paths.scripts)
		.pipe(newer('public/js/all.min.js'))
		.pipe(sourcemaps.init())
			.pipe(uglify())
			.pipe(concat('all.min.js'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('public/js'));
});

// Copy all static images
gulp.task('images', function() {
	gulp.src(paths.images, {base: 'app/images'})
		.pipe(newer('public/img'))
		.pipe(imagemin({optimizationLevel: 5}))
		.pipe(gulp.dest('public/img'));
});

gulp.task('phpunit', function() {
	gulp.src(paths.tests)
		.pipe(plumber())
		.pipe(phpunit());
});

// Rerun the task when a file changes
gulp.task('watch', function() {
	gulp.watch(paths.scripts, ['scripts']);
	gulp.watch(paths.images, ['images']);
	gulp.watch(paths.tests, ['phpunit']);
	gulp.watch(paths.sass, ['sass']);
});

gulp.task('build', ['scripts', 'images', 'sass']);

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'phpunit', 'scripts', 'images', 'sass']);
