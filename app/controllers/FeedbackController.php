<?php

class FeedbackController extends \BaseController {

	/**
	 * Store a newly created resource in storage.
	 * POST /feedback
	 *
	 * @return Response
	 */
	public function store()
	{
		$feedback = new Feedback;
		if(Auth::check()) {
			$feedback->user = Auth::user()->username;
		} else {
			$feedback->user = 'Guest';
		}
		$feedback->location = Input::get('location');
		$feedback->type = Input::get('category');
		$feedback->feedback = Input::get('feedback');
		$feedback->save();
		return Redirect::back();
	}
}