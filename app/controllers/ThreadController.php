<?php

class ThreadController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /thread
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /thread/create
	 *
	 * @return Response
	 */
	public function create($forum_id)
	{
		$forum = Forum::findOrFail($forum_id);
		return View::make('forum.new_thread')->with('forum', $forum)->with('breadcrumbs', Breadcrumbs::forum($forum));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /thread
	 *
	 * @return Response
	 */
	public function store($forum_id)
	{
		$forum = Forum::findOrFail($forum_id);
		$inputs = array(
			'post_content' => Input::get('post_content'),
			'thread_title' => Input::get('thread_title')
		);
		$rules = array(
			'post_content' => 'required|min:3',
			'thread_title' => 'required|min:3'
		);
		$validator = Validator::make($inputs, $rules);
		if($validator->fails()) {
			Session::flash('error', $validator->messages()->all());
			return Redirect::back()->withInput();
		}
		$user = Auth::user();
		$thread = new Thread;
		$thread->title = Input::get('thread_title');
		$thread->content = Input::get('post_content');
		$thread->locked_by_user_id = 0;
		$thread->views = 0;
		$thread->user()->associate($user);
		$thread->forum()->associate($forum);
		if($thread->save()) {
			Session::flash('success', array('Thread created!'));
		} else {
			Session::flash('error', array('An error occured while creating your thread.'));
			Redirect::back()->withInput();
		}
		return Redirect::to('/community/forum/' . $forum_id . '/thread/' . $thread->id);
	}

	/**
	 * Display the specified resource.
	 * GET /thread/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($forum_id, $thread_id)
	{
		$forum = Forum::findOrFail($forum_id);
		$thread = $forum->threads()->where('id', '=', $thread_id)->firstOrFail();
		$posts = $thread->posts()->get();

		$markdown = new Parsedown();
		$thread->view();
		return View::make('forum.view_thread')->with('thread', $thread)->with('posts', $posts)->with('breadcrumbs', Breadcrumbs::thread($thread))->with('markdown', $markdown);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /thread/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($forum_id, $thread_id)
	{
		$forum = Forum::findOrFail($forum_id);
		$thread = $forum->threads()->where('id', '=', $thread_id)->firstOrFail();
		$user = $thread->user;
		return View::make('forum.edit_thread')->with('thread', $thread)->with('breadcrumbs', Breadcrumbs::thread($thread));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /thread/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($forum_id, $thread_id)
	{
		$forum = Forum::findOrFail($forum_id);
		$thread = $forum->threads()->where('id', '=', $thread_id)->firstOrFail();
		$user = $thread->user()->first();
		$inputs = array(
			'post_content' => Input::get('post_content')
		);
		$rules = array(
			'post_content' => 'required|min:3'
		);
		$validator = Validator::make($inputs, $rules);
		if($validator->fails()) {
			Session::flash('error', $validator->messages()->all());
			return Redirect::back()->withInput();
		}
		$thread->timestamps = false;
		$thread->content = Input::get('post_content');
		if($thread->save()) {
			Session::flash('success', array('Thread updated.'));
		} else {
			Session::flash('error', array('A problem occured while updating the thread.'));
			return Redirect::back()->withInput();
		}
		return Redirect::to('/community/forum/' . $forum_id . '/thread/' . $thread_id);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /thread/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
