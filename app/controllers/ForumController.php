<?php

class ForumController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /forum
	 *
	 * @return Response
	 */
	public function index()
	{
		$forums = Forum::all();
		return View::make('forum.forum_index')->with('forums', $forums)->with('breadcrumbs', Breadcrumbs::forum_index());
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /forum/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /forum
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /forum/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($forum_id)
	{
		$forum = Forum::findOrFail($forum_id);
		$threads = $forum->threads()->orderBy('updated_at', 'desc')->get();
		return View::make('forum.view_forum')->with('forum', $forum)->with('threads', $threads)->with('breadcrumbs', Breadcrumbs::forum($forum));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /forum/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /forum/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /forum/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
