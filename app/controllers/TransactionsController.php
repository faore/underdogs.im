<?php

class TransactionsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /transactions
	 *
	 * @return Response
	 */
	public function index()
	{
		$transactions = Transaction::all();
		$out = '';
		$table = Transaction::getStackedAreaData();
		$out = $out . '[';
		foreach($table[0] as $header) {
			$out = $out .  '\'' . $header . '\', ';
		}
		$out = $out . '],';
		foreach($table[1] as $dataset) {
			$out = $out . '[';
			$i = 0;
			foreach($dataset as $datapoint) {
				if($i == 0) {
					$out = $out . '\'' . $datapoint . '\', ';
				} else {
					$out = $out . $datapoint . ', ';
				}
				$i++;
			}
			$out = $out . '],';
		}
		return View::make('treasury.transactions')->with('transactions', $transactions)->with('out', $out)->with('breadcrumbs', Breadcrumbs::home());
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /transactions/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /transactions
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /transactions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /transactions/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /transactions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /transactions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
