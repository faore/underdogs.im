<?php



/**
 * UsersController Class
 *
 * Implements actions regarding user management
 */
class UsersController extends Controller
{

	/**
	 * Displays the form for account creation
	 *
	 * @return  Illuminate\Http\Response
	 */
	public function create() {
		return View::make('views.register');
	}

	/**
	 * Stores new account
	 *
	 * @return  Illuminate\Http\Response
	 */
	public function store()
	{
		$inputs = array(
			'username'	=> Input::get('username'),
			'password'	=> Input::get('password'),
			'password_confirm'	=> Input::get('password_confirm'),
			'email'			=> Input::get('email'),
			'bot_check'	=> Input::get('bot_check'),
			//'rules'			=> Input::get('rules'),
			'character_name' => Input::get('character_name'),
			'class'		=> Input::get('class'),
			'age'				=> Input::get('age'),
			'past_guild_experience'	=> Input::get('past_guild_experience'),
			'five_words'	=>	Input::get('five_words'),
			'where_did_you_hear_of_us' => Input::get('where_did_you_hear_of_us'),
			'anything_else'	=> Input::get('anything_else')
		);
		$rules = array(
			'username'	=> 'required|min:3|alpha_num|unique:users,username',
			'password'	=> 'required|min:6|same:password_confirm',
			'password_confirm' => 'required|min:6|same:password_confirm',
			'email'			=>	'required|email|unique:users,email',
			'bot_check'	=>	'required|in:mirakell,Mirakell,myra,Myra,mira,Mira',
			//'rules'			=>	'accepted',
			'character_name' => 'required|min:3|alpha',
			'class'		=>	'required',
			'age'				=>	'required|integer|between:16,99',
			'past_guild_experience'	=> 'required|min:100',
			'five_words' => 'required|min:20',
			'where_did_you_hear_of_us' => 'required|min:3',
			'anything_else'	=> 'min:3'
		);
		$validator = Validator::make($inputs, $rules);
		if($validator->fails()) {
			Session::flash('error', $validator->messages()->all());
			return Redirect::back()->withInput();
		}
		$user = new User;
		$user->username = Input::get('username');
		$user->email = Input::get('email');
		$user->password = Hash::make(Input::get('password'));
		$user->blocked = 2;
		$user->activation = str_replace('/', '', Hash::make($user->id . $user->email));
		$user->save();
		$user->attachRole(5);
		$activationlink = $user->activation;
		$thread = new Thread;
		$thread->title = '[RegistrationSystem] Application';
		$thread->content = View::make('tmpl.application')->render();
		$thread->locked = 0;
		$thread->views = 0;
		$thread->user()->associate($user);
		$thread->forum()->associate(Forum::find(6));
		$thread->save();
		Mail::send('emails.welcome', array('user' => $user, 'activationlink' => $activationlink), function($message) use ($user)
		{
			$message->to($user->email, $user->username)->subject('Welcome, ' . ucfirst($user->username) . '! Activate your UnderdogsID.');
		});
		Session::flash('success', array('Your account and application were created! Check your email to activate your account.'));
		return Redirect::to('/');
	}
	/**
	 * Displays the login form
	 *
	 * @return  Illuminate\Http\Response
	 */
	public function login()
	{
		if (Auth::check()) {
			return Redirect::to('/');
		} else {
			return View::make('views.login');
		}
	}

	/**
	 * Attempt to do login
	 *
	 * @return  Illuminate\Http\Response
	 */
	public function doLogin()
	{
		$provid = Input::get('username');
		$useritem = User::where('username', '=', $provid)->first();
		$rules = array(
			'username'    => 'required|min:3', // make sure the email is an actual email
			'password' => 'required|min:3' // password can only be alphanumeric and has to be greater than 3 characters
		);
		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);
		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('/users/login?e=1')->withInput(Input::except('password'));
		} else {
			// create our user data for the authentication
			$userdata = array(
				'username'     => Input::get('username'),
				'password'  => Input::get('password'),
				'blocked' => 0
			);
			// attempt to do the login
			if (Auth::attempt($userdata)) {
				Session::flash('success', array('Welcome back, ' . $useritem->username . '!'));
				return Redirect::intended('');
			} else {
				if($useritem && $useritem->blocked == 3) {
					Session::flash('error', array('Your application was rejected. To reapply, contact an officer.'));
					return Redirect::to('/users/login')->withInput(Input::except('password'));
				}
				if($useritem && $useritem->blocked == 2) {
					return Redirect::to('/users/login?e=3')->withInput(Input::except('password'));
				}
				elseif($useritem && $useritem->blocked == 1) {
					return Redirect::to('/users/login?e=2')->withInput(Input::except('password'));
				} else {
					return Redirect::to('/users/login?e=1')->withInput(Input::except('password'));
				}
			}
		}
	}

	/**
	 * Attempt to confirm account with code
	 *
	 * @param  string $code
	 *
	 * @return  Illuminate\Http\Response
	 */
	public function confirm($id, $code)
	{
		$user = User::findOrFail($id);
		echo '<p>' . $code .'</p>';
		echo '<p>' . str_replace('/', '', Hash::make($user->id . $user->email)) . '</p>';
		if($user->activation == $code) {
			if($user->blocked == 2) {
				$user->blocked = 0;
				$user->save();
				Auth::login($user);
				Session::flash('success', array('Congratulations! Your UnderdogsID was activated, we\'ve logged you in automatically.'));
				if($user->hasRole("Underdog") || $user->hasRole("Watchdog") || $user->hasRole("Server Admin") || $user->hasRole("Topdog")) {
					Session::flash('success', array_push(Session::get('success'), 'Your application was also already accepted! Welcome to the Underdogs Guild!'));
				}
				Session::flash('info', array('Your application hasn\'t been approved yet. You can find your application on the forum.'));
				return Redirect::to('/');
			} else {
				//Already Activated
			}
		}
	}

	/**
	 * Displays the forgot password form
	 *
	 * @return  Illuminate\Http\Response
	 */
	public function forgotPassword()
	{
		return View::make(Config::get('confide::forgot_password_form'));
	}

	/**
	 * Attempt to send change password link to the given email
	 *
	 * @return  Illuminate\Http\Response
	 */
	public function doForgotPassword()
	{
		if (Confide::forgotPassword(Input::get('email'))) {
			$notice_msg = Lang::get('confide::confide.alerts.password_forgot');
			return Redirect::action('UsersController@login')
				->with('notice', $notice_msg);
		} else {
			$error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');
			return Redirect::action('UsersController@forgot_password')
				->withInput()
				->with('error', $error_msg);
		}
	}

	/**
	 * Shows the change password form with the given token
	 *
	 * @param  string $token
	 *
	 * @return  Illuminate\Http\Response
	 */
	public function resetPassword($token)
	{
		return View::make(Config::get('confide::reset_password_form'))
				->with('token', $token);
	}

	/**
	 * Attempt change password of the user
	 *
	 * @return  Illuminate\Http\Response
	 */
	public function doResetPassword()
	{
		$repo = App::make('UserRepository');
		$input = array(
			'token'                 =>Input::get('token'),
			'password'              =>Input::get('password'),
			'password_confirmation' =>Input::get('password_confirmation'),
		);

		// By passing an array with the token, password and confirmation
		if ($repo->resetPassword($input)) {
			$notice_msg = Lang::get('confide::confide.alerts.password_reset');
			return Redirect::action('UsersController@login')
				->with('notice', $notice_msg);
		} else {
			$error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
			return Redirect::action('UsersController@reset_password', array('token'=>$input['token']))
				->withInput()
				->with('error', $error_msg);
		}
	}

	/**
	 * Log the user out of the application.
	 *
	 * @return  Illuminate\Http\Response
	 */
	public function logout()
	{
		Auth::logout();

		return Redirect::to('/');
	}
}
