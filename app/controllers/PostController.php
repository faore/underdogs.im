<?php

class PostController extends \BaseController {
	/**
	 * Show the form for creating a new resource.
	 * GET /post/create
	 *
	 * @return Response
	 */
	public function create($forum_id, $thread_id)
	{
		$forum = Forum::findOrFail($forum_id);
		$thread = $forum->threads()->where('id', '=', $thread_id)->firstOrFail();
		return View::make('forum.new_post')->with('thread', $thread)->with('breadcrumbs', Breadcrumbs::thread($thread));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /post
	 *
	 * @return Response
	 */
	public function store($forum_id, $thread_id)
	{
		$forum = Forum::findOrFail($forum_id);
		$thread = $forum->threads()->where('id', '=', $thread_id)->firstOrFail();
		$inputs = array('post_content' => Input::get('post_content'));
		$rules = array('post_content' => 'required|min:3');
		$validator = Validator::make($inputs, $rules);
		if($validator->fails()) {
			Session::flash('error', $validator->messages()->all());
			return Redirect::back()->withInput();
		}
		$user = Auth::user();
		$post = new Post;
		$post->locked_by_user_id = 0;
		$post->edited_by_user_id = 0;
		$post->content = Input::get('post_content');
		$post->user()->associate($user);
		$post->thread()->associate($thread);
		$thread->touch();
		if($post->save()) {
			Session::flash('success', array('Post created.'));
		} else {
			Session::flash('error', array('An error occured while creating your post.'));
			return Redirect::back()->withInput();
		}
		return Redirect::to('/community/forum/' . $forum_id . '/thread/' . $thread_id . '/#post_' . $post->id);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /post/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($forum_id, $thread_id, $post_id)
	{
		$forum = Forum::findOrFail($forum_id);
		$thread = $forum->threads()->where('id', $thread_id)->firstOrFail();
		$post = $thread->posts()->where('id', $post_id)->firstOrFail();
		$user = $post->user;
		return View::make('forum.edit_post')->with('breadcrumbs', Breadcrumbs::thread($thread))->with('thread', $thread)->with('post', $post);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /post/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($forum_id, $thread_id, $post_id)
	{
		$forum = Forum::findOrFail($forum_id);
		$thread = $forum->threads()->where('id', $thread_id)->firstOrFail();
		$post = $thread->posts()->where('id', $post_id)->firstOrFail();
		$user = $post->user;
		$inputs = array('post_content' => Input::get('post_content'));
		$rules = array('post_content' => 'required|min:3');
		$validator = Validator::make($inputs, $rules);
		if($validator->fails()) {
			Session::flash('error', $validator->messages()->all());
			return Redirect::back()->withInput();
		}
		$post->content = Input::get('post_content');
		if($post->save()) {
			Session::flash('success', array('Post updated.'));
		} else {
			Session::flash('error', array('A problem occured while updating the post.'));
			return Redirect::back()->withInput();
		}
		return Redirect::to('/community/forum/' . $forum_id . '/thread/' . $thread_id . '/#post_' . $post_id);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /post/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
