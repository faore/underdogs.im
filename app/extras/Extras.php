<?php

class Extras {
	public static function random_salutation($username) {
		$username = ucfirst($username);
		$salutations = array(
			'Hello, ' . $username . '!',
			'Welcome, ' . $username . '!',
			'\'Sup, ' . $username . '!',
			'Aloha, ' . $username . '!',
			'Bienvenido, ' . $username . '!',
			'Willkommen, ' . $username . '!',
		);
		return $salutations[array_rand($salutations, 1)];
	}
}
