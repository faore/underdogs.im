<?php
class CharityTracker {
  public static function getGoalInfo() {
    if (Cache::has('CharityInfo')) {
	return Cache::get('CharityInfo');
    }
    $url = 'https://api.justgiving.com/d15934a8/v1/fundraising/pages/UnderdogsCharityStream';
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
    $data = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($data);
    if(isset($result)) {
      $array = array($result->grandTotalRaisedExcludingGiftAid, $result->fundraisingTarget);
    } else {
      $array = array(0,0);
    }
    Cache::put('CharityInfo', $array, 10);
    return $array;
  }
}
