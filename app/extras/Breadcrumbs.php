<?php
class Breadcrumbs {
	public static function build($breadcrumbs) {
		return View::make('tmpl.breadcrumbs')->with('breadcrumbs', $breadcrumbs)->render();
	}
	public static function home() {
		return '<li class="uk-active">Home</li>';
	}
	public static function forum($forum) {
		return '<li><span>Community</span></li><li><span><a href="/community/forum">Forum</a></span></li><li class="uk-active"><span>' . $forum->name . '</span></li>';
	}
	public static function thread($thread) {
		return '<li><span>Community</span></li><li><span><a href="/community/forum">Forum</a></span></li><li><a href="/community/forum/' . $thread->forum->id . '">' . $thread->forum->name . '</a></li><li class="uk-active"><span>' . $thread->title . '</span></li>';
	}
	public static function teamspeak() {
		return '<li><span>Servers</span></li><li class="uk-active"><span>TeamSpeak</span></li>';
	}
	public static function forum_index() {
		return '<li><span>Community</span></li><li class="uk-active"><span>Forum</span></li>';
	}
}
