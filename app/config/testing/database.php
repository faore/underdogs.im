<?php
return array(
  'default' => 'travis',
  'connections' => array(
    'testing' => array(
      'driver'   => 'mysql',
      'host'     => 'cranberry.admt.im',
      'database' => 'uds-testing',
      'username' => 'testing',
      'password' => 'Testing!',
      'charset'  => 'utf8',
      'prefix'   => 'udsl_',
      'collation'=> 'utf8_unicode_ci',
    ),
    'local-testing' => array(
      'driver'    => 'mysql',
      'host'      => 'localhost',
      'database'  => 'uds-testing',
      'username'  => 'root',
      'password'  => 'C00lwing!',
      'charset'   => 'utf8',
      'collation' => 'utf8_unicode_ci',
      'prefix'    => 'udsl_',
    ),
    'travis' => array(
      'driver'    => 'mysql',
      'host'      => 'localhost',
      'database'  => 'testing',
      'username'  => 'root',
      'password'  => '',
      'charset'   => 'utf8',
      'collation' => 'utf8_unicode_ci',
      'prefix'    => 'udsl_',
    ),
  ),
);
