<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Database Connections
	|--------------------------------------------------------------------------
	|
	| Here are each of the database connections setup for your application.
	| Of course, examples of configuring each database platform that is
	| supported by Laravel is shown below to make development simple.
	|
	|
	| All database work in Laravel is done through the PHP PDO facilities
	| so make sure you have the driver for your particular database of
	| choice installed on your machine before you begin development.
	|
	*/
	'default'	=> 'devel',

	'connections' => array(

		'devel' => array(
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'uds-laravel',
			'username'  => 'root',
			'password'  => 'C00lwing!',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => 'udsl_',
		),

		'sqlite' => array(
			'driver' => 'sqlite',
			'database' => __DIR__.'/../../database/testing.sqlite',
			'prefix' => '',
		),
	),

);
