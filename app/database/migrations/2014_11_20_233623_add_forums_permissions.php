<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForumsPermissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('forums', function($table)
		{
			$table->dropColumn('viewlevel');
			$table->dropColumn('postlevel');
			// General Permissions
			$table->integer('view_permission_id')->default(1);
			$table->integer('post_permission_id')->default(1);
			$table->integer('lock_permission_id')->default(1);
			$table->integer('sticky_permission_id')->default(1);
			$table->integer('delete_permission_id')->default(1);
			$table->integer('move_permission_id')->default(1);
			$table->integer('edit_permission_id')->default(1);
			$table->integer('post_in_locked_permission_id')->default(1);
			// Own Permissions
			$table->integer('lock_own_permission_id')->default(1);
			$table->integer('delete_own_permission_id')->default(1);
			$table->integer('edit_own_permission_id')->default(1);
			$table->integer('move_own_permission_id')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('forums', function($table)
		{
			$table->integer('viewlevel')->default(1);
			$table->integer('postlevel')->default(1);
			// General Permissions
			$table->dropColumn('view_permission_id');
			$table->dropColumn('post_permission_id');
			$table->dropColumn('lock_permission_id');
			$table->dropColumn('sticky_permission_id');
			$table->dropColumn('delete_permission_id');
			$table->dropColumn('move_permission_id');
			$table->dropColumn('edit_permission_id');
			$table->dropColumn('post_in_locked_permission_id');
			// Own Permissions
			$table->dropColumn('lock_own_permission_id');
			$table->dropColumn('delete_own_permission_id');
			$table->dropColumn('edit_own_permission_id');
			$table->dropColumn('move_own_permission_id');
		});
	}

}
