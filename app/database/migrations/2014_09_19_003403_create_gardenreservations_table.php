<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGardenreservationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gardenreservations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id'); //Belongs to a user
			$table->integer('queue'); //0 = In queue, 1 = request completed, 2 = has plot
			$table->integer('patch'); // Represent patches 1-3
			$table->integer('paid'); // 0 if unpaid, 1 if paid. People shouldn't pay until they are ready to recieve their plot.
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gardenreservations');
	}

}
