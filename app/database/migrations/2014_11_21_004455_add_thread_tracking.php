<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddThreadTracking extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('threads', function($table)
		{
			$table->dropColumn('locked');
			$table->integer('locked_by_user_id')->nullable();
			$table->integer('edited_by_user_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('threads', function($table)
		{
			$table->integer('locked');
			$table->dropColumn('locked_by_user_id');
			$table->dropColumn('edited_by_user_id');
		});
	}

}
