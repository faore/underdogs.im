<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRolePermissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('roles', function($table)
		{
			$table->integer('edit_this_posts_permission_id')->default(1);
			$table->integer('delete_this_posts_permission_id')->default(1);
			$table->integer('lock_this_posts_permission_id')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('roles', function($table)
		{
			$table->dropColumn('edit_this_posts_permission_id');
			$table->dropColumn('delete_this_posts_permission_id');
			$table->dropColumn('lock_this_posts_permission_id');
		});
	}

}
