<?php
class Beta1Update1 extends Seeder {

	public function run()
	{
		$permission = New Permission;
		$permission->name = 'PostLevel_0';
		$permission->display_name = 'Post in Level 0 Forums';
		$permission->save();

		Role::find(3)->perms()->sync(array(1,2,3,4,7,10, $permission->id)); //Watchdog
		Role::find(4)->perms()->sync(array(1,3, $permission->id)); //Underdog
		Role::find(5)->perms()->sync(array($permission->id)); //Applicant
	}

}