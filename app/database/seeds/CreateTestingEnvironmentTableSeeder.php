<?php

class CreateTestingEnvironmentTableSeeder extends Seeder {

	public function run() {
		Eloquent::unguard();
		//Create the basic roles.
		Role::create(array('id' => '1', 'name' => 'Server Admin'));
		Role::create(array('id' => '2', 'name' => 'Topdog'));
		Role::create(array('id' => '3', 'name' => 'Watchdog'));
		Role::create(array('id' => '4', 'name' => 'Underdog'));
		Role::create(array('id' => '5', 'name' => 'Applicant'));
		//Create some users.
		User::create(array('id' => '1', 'username' => 'ServerAdmin', 'email' => 'serveradmin.test@faore.in', 'password' => Hash::make('test'), 'blocked' => 0));
		User::create(array('id' => '2', 'username' => 'Topdog', 'email' => 'topdog.test@faore.in', 'password' => Hash::make('test'), 'blocked' => 0));
		User::create(array('id' => '3', 'username' => 'Watchdog', 'email' => 'watchdog.test@faore.in', 'password' => Hash::make('test'), 'blocked' => 0));
		User::create(array('id' => '4', 'username' => 'Underdog', 'email' => 'underdog.test@faore.in', 'password' => Hash::make('test'), 'blocked' => 0));
		User::create(array('id' => '5', 'username' => 'Applicant', 'email' => 'serveradmin.test@faore.in', 'password' => Hash::make('test'), 'blocked' => 0));
		User::create(array('id' => '6', 'username' => 'UnitTest', 'email' => 'unittest.test@faore.in', 'password' => Hash::make('test'), 'blocked' => 0));
		//Attach Roles to users (UnitTest gets no role.)
		User::find(1)->roles()->attach(Role::find(1));
		User::find(2)->roles()->attach(Role::find(2));
		User::find(3)->roles()->attach(Role::find(3));
		User::find(4)->roles()->attach(Role::find(4));
		User::find(5)->roles()->attach(Role::find(5));
		//Create A Forum
		Forum::create(array('id' => '1', 'name' => 'UnitTest', 'description' => 'Unit testing is fun.'));
		//Create A Thread
		Thread::create(array('id' => '1', 'forum_id' => '1', 'user_id' => '1', 'title' => 'Unit Testing!', 'content' => 'Is the freaking best.'));
		Post::create(array('id' => '1', 'thread_id' => '1', 'user_id' => '1', 'content' => 'SA post.', 'locked_by_user_id' => 0, 'edited_by_user_id' => 0));
		Post::create(array('id' => '2', 'thread_id' => '1', 'user_id' => '2', 'content' => 'TD post.','locked_by_user_id' => 0, 'edited_by_user_id' => 0));
		Post::create(array('id' => '3', 'thread_id' => '1', 'user_id' => '3', 'content' => 'WD post.','locked_by_user_id' => 0, 'edited_by_user_id' => 0));
		Post::create(array('id' => '4', 'thread_id' => '1', 'user_id' => '4', 'content' => 'UD post.','locked_by_user_id' => 0, 'edited_by_user_id' => 0));
		Post::create(array('id' => '5', 'thread_id' => '1', 'user_id' => '5', 'content' => 'AP post.','locked_by_user_id' => 0, 'edited_by_user_id' => 0));
		Post::create(array('id' => '6', 'thread_id' => '1', 'user_id' => '6', 'content' => 'NR post.','locked_by_user_id' => 0, 'edited_by_user_id' => 0));
		//Some More (Empty) Threads
		Thread::create(array('id' => '2', 'forum_id' => '1', 'user_id' => '2', 'title' => 'TDI have...', 'content' => 'The best thread.'));
		Thread::create(array('id' => '3', 'forum_id' => '1', 'user_id' => '3', 'title' => 'WDI have...', 'content' => 'The best thread.'));
		Thread::create(array('id' => '4', 'forum_id' => '1', 'user_id' => '4', 'title' => 'USI have...', 'content' => 'The best thread.'));
		Thread::create(array('id' => '5', 'forum_id' => '1', 'user_id' => '5', 'title' => 'AppI have...', 'content' => 'The best thread.'));
		Thread::create(array('id' => '6', 'forum_id' => '1', 'user_id' => '6', 'title' => 'NoRoleI have...', 'content' => 'The best thread.'));
		//Locked Thread
		Thread::create(array('id' => '7', 'forum_id' => '1', 'user_id' => '4', 'title' => 'Unit Testing!', 'content' => 'Is the freaking best.', 'locked_by_user_id' => 1));
		$permissionsSeeder = New NewPermissionsTableSeeder;
		$permissionsSeeder->run();
		//Sync Permissions for roles.
		Role::find(1)->perms()->sync(array(Permission::find(1)->id));
		$topdogPerms = array();
		foreach(Permission::all() as $perm) {
			$topdogPerms[] = $perm->id;
		}
		Role::find(2)->perms()->sync($topdogPerms);
		Role::find(3)->perms()->sync(array(10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28));
		Role::find(4)->perms()->sync(array(17,18,25,26,27,28));
		//Role::find(5)->perms()->sync(array(17));
	}

}
