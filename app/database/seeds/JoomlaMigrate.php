<?php

class JoomlaMigrate extends Seeder {
	private static function mapRole($user_id, $joomla_role) {
		switch($joomla_role) {
			case 2:
				//Applicant
				$role_id = 5;
				break;
			case 6:
				//Watchdog
				$role_id = 3;
				break;
			case 7:
				//Topdog
				$role_id = 2;
				break;
			case 8:
				//Server Admin
				$role_id = 1;
				break;
			case 10:
				//Underdog
				$role_id = 4;
				break;
			default:
				$role_id = 5; //Default to Applicant
				break;
		}
		DB::table('assigned_roles')->insert(array('user_id' => $user_id, 'role_id' => $role_id));
	}
	public function run() {
		$post_thread = array();
		Eloquent::unguard();
		Role::create(array('name' => 'Server Admin'));
		Role::create(array('name' => 'Topdog'));
		Role::create(array('name' => 'Watchdog'));
		Role::create(array('name' => 'Underdog'));
		Role::create(array('name' => 'Applicant'));
		Slide::create(array('name' => 'Initial Slide 1', 'title' => 'Coil-Ready!', 'url' => '/img/slides/starter/slide1.png', 'description' => 'Members of Underdogs equipped and ready to take on Coil.', 'active' => 1));
		Slide::create(array('name' => 'Initial Slide 2', 'title' => 'Draco Streams the Praetorium!', 'url' => '/img/slides/starter/slide2.png', 'description' => 'Watch it now on <a href=\'http://www.youtube.com/watch?v=U9NDvhmHbbA\'>YouTube</a>.', 'active' => 1));
		//Slide::create(array('name' => 'Initial Slide 3', 'title' => 'Starbound!', 'url' => '/img/slides/starter/slide3.png', 'description' => 'The new Starbound Server is ready to be played on! <a href=\'#\'>Check it out.</a>', 'active' => 1));
		// Permissions
		Permission::create(array('name' => 'ViewLevel_1', 'display_name' => 'View Level 1 Forums')); //1
		Permission::create(array('name' => 'ViewLevel_2', 'display_name' => 'View Level 2 Forums')); //2
		Permission::create(array('name' => 'PostLevel_1', 'display_name' => 'Post in Level 1 Forums')); //3
		Permission::create(array('name' => 'PostLevel_2', 'display_name' => 'Post in Level 2 Forums')); //4
		Permission::create(array('name' => 'ViewAllLevels', 'display_name' => 'View All Forms Regaurdless of Level')); //5
		Permission::create(array('name' => 'PostAllLevels', 'display_name' => 'View All Forms Regaurdless of Level')); //6
		Permission::create(array('name' => 'EditUnderdogsPost', 'display_name' => 'Edit Posts Belonging to Underdogs')); //7
		Permission::create(array('name' => 'EditWatchdogsPost', 'display_name' => 'Edit Posts Belonging to Watchdogs')); //8
		Permission::create(array('name' => 'EditPostsUnrestricted', 'display_name' => 'Edit All Posts and Create Posts (Ignoring Rank)')); //9
		Permission::create(array('name' => 'AdministrateWebsite', 'display_name' => 'Login to Adminstration and Change Anything (!)')); //10
		// Assign Permissions to Role
		Role::find(1)->perms()->sync(array(5,6,9,10)); //Server Admin
		Role::find(2)->perms()->sync(array(5,6,9,10)); //Topdog
		Role::find(3)->perms()->sync(array(1,2,3,4,7,10)); //Watchdog
		Role::find(4)->perms()->sync(array(1,3)); //Underdog
		Role::find(5)->perms()->sync(array()); //Applicant
		// User Migration
		$users = DB::connection('old_db')->table('users')->get();
		foreach($users as $user) {
			DB::table('users')->insert(array('id' => $user->id, 'username' => $user->username, 'email' => $user->email, 'password' => $user->password, 'blocked' => $user->block, 'created_at' => $user->registerDate, 'updated_at' => date('Y-m-d H:i:s', time())));
		}
		unset($user);
		// Forum Category Migration
		$categories = DB::connection('old_db')->table('kunena_categories')->get();
		foreach($categories as $category) {
			DB::table('forums')->insert(array('id' => $category->id, 'name' => $category->name, 'description' => $category->description . ' ', 'viewlevel' => 0, 'postlevel' => 0, 'created_at' => date('Y-m-d H:i:s', time()), 'updated_at' => date('Y-m-d H:i:s', time())));
		}
		unset($categories);
		// Thread Migration
		$threads = DB::connection('old_db')->table('kunena_topics')->get();
		foreach($threads as $thread) {
			DB::table('threads')->insert(array('id' => $thread->id, 'forum_id' => $thread->category_id, 'user_id' => $thread->first_post_userid, 'title' => $thread->subject, 'content' => $thread->first_post_message, 'locked' => $thread->locked, 'views' => $thread->hits, 'created_at' => date('Y-m-d H:i:s', $thread->first_post_time), 'updated_at' => date('Y-m-d H:i:s', $thread->last_post_time)));
			array_push($post_thread, $thread->first_post_id);
		}
		unset($threads);
		// Map Roles to Users
		$usergroups = DB::connection('old_db')->table('user_usergroup_map')->get();
		foreach($usergroups as $usergroup) {
			JoomlaMigrate::mapRole($usergroup->user_id, $usergroup->group_id);
		}
		unset($usergroups);
		// Post Migration
		$posts = DB::connection('old_db')->table('kunena_messages')->get();
		foreach($posts as $post) {
			if(!in_array($post->id, $post_thread)) {
				$content = DB::connection('old_db')->table('kunena_messages_text')->where('mesid', $post->id)->first();			
				DB::table('posts')->insert(array('id' => $post->id, 'thread_id' => $post->thread, 'user_id' => $post->userid, 'content' => $content->message, 'created_at' => date('Y-m-d H:i:s', $post->time), 'updated_at' => date('Y-m-d H:i:s', $post->modified_time)));
			}
		}
		unset($posts, $content);
	}
}