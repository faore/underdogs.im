<?php

class NewPermissionsTableSeeder extends Seeder {

	public function run()
	{
		Eloquent::unguard();
		// This will create all the permissions required to move from the old
		// permission system to the new system.

		// Remove current Permissions, and associations.
		DB::table('permission_role')->delete();
		DB::table('permissions')->delete();
		Permission::create(
			array(
				'id' => 1,
				'name' => 'IgnorePermissions',
				'display_name' => 'Ignore Permissions',
				'type' => 'General'
			)
		);

		// Role Comparision Permissions

		//Server Admin
		Permission::create(
			array(
				'id' => 2,
				'name' => 'EditPostsBelongingToRole_1',
				'display_name' => 'Edit Posts Belonging to Server Admins',
				'type' => 'Forum'
			)
		);

		Permission::create(
			array(
				'id' => 3,
				'name' => 'DeletePostsBelongingToRole_1',
				'display_name' => 'Delete Posts Belonging to Server Admins',
				'type' => 'Forum'
			)
		);

		Permission::create(
			array(
				'id' => 4,
				'name' => 'LockPostsBelongingToRole_1',
				'display_name' => 'Lock Posts Belonging to Server Admins',
				'type' => 'Forum'
			)
		);
		DB::table('roles')
    ->where('id', 1)
    ->update(
			array(
				'edit_this_posts_permission_id' => 2,
				'delete_this_posts_permission_id' => 3,
				'lock_this_posts_permission_id' => 4
			)
		);

		//Topdog
		Permission::create(
			array(
				'id' => 5,
				'name' => 'EditPostsBelongingToRole_2',
				'display_name' => 'Edit Posts Belonging to Topdogs',
				'type' => 'Forum'
			)
		);

		Permission::create(
			array(
				'id' => 6,
				'name' => 'DeletePostsBelongingToRole_2',
				'display_name' => 'Delete Posts Belonging to Topdogs',
				'type' => 'Forum'
			)
		);

		Permission::create(
			array(
				'id' => 7,
				'name' => 'LockPostsBelongingToRole_2',
				'display_name' => 'Lock Posts Belonging to Topdogs',
				'type' => 'Forum'
			)
		);
		DB::table('roles')
		->where('id', 2)
		->update(
			array(
				'edit_this_posts_permission_id' => 5,
				'delete_this_posts_permission_id' => 6,
				'lock_this_posts_permission_id' => 7
			)
		);

		//Watchdog
		Permission::create(
			array(
				'id' => 8,
				'name' => 'EditPostsBelongingToRole_3',
				'display_name' => 'Edit Posts Belonging to Watchdogs',
				'type' => 'Forum'
			)
		);

		Permission::create(
			array(
				'id' => 9,
				'name' => 'DeletePostsBelongingToRole_3',
				'display_name' => 'Delete Posts Belonging to Watchdogs',
				'type' => 'Forum'
			)
		);

		Permission::create(
			array(
				'id' => 10,
				'name' => 'LockPostsBelongingToRole_3',
				'display_name' => 'Lock Posts Belonging to Watchdogs',
				'type' => 'Forum'
			)
		);
		DB::table('roles')
		->where('id', 3)
		->update(
			array(
				'edit_this_posts_permission_id' => 8,
				'delete_this_posts_permission_id' => 9,
				'lock_this_posts_permission_id' => 10
			)
		);

		//Underdog
		Permission::create(
			array(
				'id' => 11,
				'name' => 'EditPostsBelongingToRole_4',
				'display_name' => 'Edit Posts Belonging to Underdogs',
				'type' => 'Forum'
			)
		);

		Permission::create(
			array(
				'id' => 12,
				'name' => 'DeletePostsBelongingToRole_4',
				'display_name' => 'Delete Posts Belonging to Underdogs',
				'type' => 'Forum'
			)
		);

		Permission::create(
			array(
				'id' => 13,
				'name' => 'LockPostsBelongingToRole_4',
				'display_name' => 'Lock Posts Belonging to Underdogs',
				'type' => 'Forum'
			)
		);
		DB::table('roles')
		->where('id', 4)
		->update(
			array(
				'edit_this_posts_permission_id' => 11,
				'delete_this_posts_permission_id' => 12,
				'lock_this_posts_permission_id' => 13
			)
		);

		//Applicant
		Permission::create(
			array(
				'id' => 14,
				'name' => 'EditPostsBelongingToRole_5',
				'display_name' => 'Edit Posts Belonging to Applicants',
				'type' => 'Forum'
			)
		);

		Permission::create(
			array(
				'id' => 15,
				'name' => 'DeletePostsBelongingToRole_5',
				'display_name' => 'Delete Posts Belonging to Applicants',
				'type' => 'Forum'
			)
		);

		Permission::create(
			array(
				'id' => 16,
				'name' => 'LockPostsBelongingToRole_5',
				'display_name' => 'Lock Posts Belonging to Applicants',
				'type' => 'Forum'
			)
		);
		DB::table('roles')
		->where('id', 5)
		->update(
			array(
				'edit_this_posts_permission_id' => 14,
				'delete_this_posts_permission_id' => 15,
				'lock_this_posts_permission_id' => 16
			)
		);

		// Create Related Forum Permissions
		$forums = Forum::all();

		foreach($forums as $forum) {
			$view = new Permission;
			$view->name = 'ViewForum_' . $forum->id;
			$view->display_name = 'View Forum ' . $forum->name;
			$view->type = 'Forum/' . $forum->name;
			$view->save();

			$post = new Permission;
			$post->name = 'PostInForum_' . $forum->id;
			$post->display_name = 'Post In Forum ' . $forum->name;
			$post->type = 'Forum/' . $forum->name;
			$post->save();

			$lock = new Permission;
			$lock->name = 'LockInForum_' . $forum->id;
			$lock->display_name = 'Lock In Forum ' . $forum->name;
			$lock->type = 'Forum/' . $forum->name;
			$lock->save();

			$sticky = new Permission;
			$sticky->name = 'StickyInForum_' . $forum->id;
			$sticky->display_name = 'Sticky In Forum ' . $forum->name;
			$sticky->type = 'Forum/' . $forum->name;
			$sticky->save();

			$delete = new Permission;
			$delete->name = 'DeleteInForum_' . $forum->id;
			$delete->display_name = 'Delete In Forum ' . $forum->name;
			$delete->type = 'Forum/' . $forum->name;
			$delete->save();

			$move = new Permission;
			$move->name = 'MoveInForum_' . $forum->id;
			$move->display_name = 'Move In Forum ' . $forum->name;
			$move->type = 'Forum/' . $forum->name;
			$move->save();

			$edit = new Permission;
			$edit->name = 'EditInForum_' . $forum->id;
			$edit->display_name = 'Edit In Forum ' . $forum->name;
			$edit->type = 'Forum/' . $forum->name;
			$edit->save();

			$post_in_locked = new Permission;
			$post_in_locked->name = 'PostInLockedInForum_' . $forum->id;
			$post_in_locked->display_name = 'Post In Locked In Forum ' . $forum->name;
			$post_in_locked->type = 'Forum/' . $forum->name;
			$post_in_locked->save();

			$lock_own = new Permission;
			$lock_own->name = 'LockOwnInForum_' . $forum->id;
			$lock_own->display_name = 'Lock Own In Forum ' . $forum->name;
			$lock_own->type = 'Forum/' . $forum->name;
			$lock_own->save();

			$delete_own = new Permission;
			$delete_own->name = 'DeleteOwnInForum_' . $forum->id;
			$delete_own->display_name = 'Delete Own In Forum ' . $forum->name;
			$delete_own->type = 'Forum/' . $forum->name;
			$delete_own->save();

			$edit_own = new Permission;
			$edit_own->name = 'EditOwnInForum_' . $forum->id;
			$edit_own->display_name = 'Edit Own In Forum ' . $forum->name;
			$edit_own->type = 'Forum/' . $forum->name;
			$edit_own->save();

			$move_own = new Permission;
			$move_own->name = 'MoveOwnInForum_' . $forum->id;
			$move_own->display_name = 'Move Own In Forum ' . $forum->name;
			$move_own->type = 'Forum/' . $forum->name;
			$move_own->save();

			$forum->viewPermission()->associate($view)->save();
			$forum->postPermission()->associate($post)->save();
			$forum->lockPermission()->associate($lock)->save();
			$forum->stickyPermission()->associate($sticky)->save();
			$forum->deletePermission()->associate($delete)->save();
			$forum->movePermission()->associate($move)->save();
			$forum->editPermission()->associate($edit)->save();
			$forum->postInLockedPermission()->associate($post_in_locked)->save();
			$forum->lockOwnPermission()->associate($lock_own)->save();
			$forum->deleteOwnPermission()->associate($delete_own)->save();
			$forum->editOwnPermission()->associate($edit_own)->save();
			$forum->moveOwnPermission()->associate($move_own)->save();
		}

	}
}
