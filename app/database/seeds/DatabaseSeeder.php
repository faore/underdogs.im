<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		User::create(array('username' => 'faore', 'email' => 'faore@faore.in', 'password' => Hash::make('secret'), 'blocked' => 0));
		Role::create(array('name' => 'Server Admin'));
		Role::create(array('name' => 'Topdog'));
		Role::create(array('name' => 'Watchdog'));
		Role::create(array('name' => 'Underdog'));
		Role::create(array('name' => 'Applicant'));
		DB::table('assigned_roles')->insert(array('user_id' => 1, 'role_id' => 1));
		DB::table('assigned_roles')->insert(array('user_id' => 1, 'role_id' => 3));
		Slide::create(array('name' => 'Initial Slide 1', 'title' => 'Coil-Ready!', 'url' => '/img/slides/starter/slide1.png', 'description' => 'Members of Underdogs equipped and ready to take on Coil.', 'active' => 1));
		Slide::create(array('name' => 'Initial Slide 2', 'title' => 'Draco Streams the Praetorium!', 'url' => '/img/slides/starter/slide2.png', 'description' => 'Watch it now on <a href=\'http://www.youtube.com/watch?v=U9NDvhmHbbA\'>YouTube</a>.', 'active' => 1));
		Slide::create(array('name' => 'Initial Slide 3', 'title' => 'Starbound!', 'url' => '/img/slides/starter/slide3.png', 'description' => 'The new Starbound Server is ready to be played on! <a href=\'#\'>Check it out.</a>', 'active' => 1));
		Forum::create(array('name' => 'General Discussion', 'description' => 'The General Things!', 'viewlevel' => 0, 'postlevel' => 0));
		Forum::create(array('name' => 'UnGeneral Discussion', 'description' => 'The UnGeneral Things!', 'viewlevel' => 0, 'postlevel' => 1));
		Forum::create(array('name' => 'Discussion', 'description' => 'The ... Things!', 'viewlevel' => 0, 'postlevel' => 2));
		Thread::create(array('user_id' => 1, 'forum_id' => 1, 'title' => 'A Thread!', 'content' => 'My thread content!', 'locked' => 0, 'views' => 3));
		Thread::create(array('user_id' => 1, 'forum_id' => 1, 'title' => 'Another Thread!', 'content' => 'My other thread content!', 'locked' => 0, 'views' => 10));
		Thread::create(array('user_id' => 1, 'forum_id' => 1, 'title' => 'A Third Thread!', 'content' => 'My third thread content!', 'locked' => 0, 'views' => 13));
		Post::create(array('user_id' => 1, 'thread_id' => 1, 'content' => 'Post content!'));
		Post::create(array('user_id' => 1, 'thread_id' => 1, 'content' => 'More Post content!'));
		Post::create(array('user_id' => 1, 'thread_id' => 1, 'content' => 'Even More Post content!'));

		// Permissions
		Permission::create(array('name' => 'ViewLevel_1', 'display_name' => 'View Level 1 Forums'));
		Permission::create(array('name' => 'ViewLevel_2', 'display_name' => 'View Level 2 Forums'));
		Permission::create(array('name' => 'PostLevel_1', 'display_name' => 'Post in Level 1 Forums'));
		Permission::create(array('name' => 'PostLevel_2', 'display_name' => 'Post in Level 2 Forums'));
		Permission::create(array('name' => 'ViewAllLevels', 'display_name' => 'View All Forms Regaurdless of Level'));
		Permission::create(array('name' => 'PostAllLevels', 'display_name' => 'View All Forms Regaurdless of Level'));
		Permission::create(array('name' => 'EditUnderdogsPost', 'display_name' => 'Edit Posts Belonging to Underdogs'));
		Permission::create(array('name' => 'EditWatchdogsPost', 'display_name' => 'Edit Posts Belonging to Watchdogs'));
		Permission::create(array('name' => 'EditPostsUnrestricted', 'display_name' => 'Edit All Posts and Create Posts (Ignoring Rank)'));
		Permission::create(array('name' => 'AdministrateWebsite', 'display_name' => 'Login to Adminstration and Change Anything (!)'));

		// Assign Permissions to Role
		Role::find(1)->perms()->sync(array(5,6,9,10)); //Server Admin
		Role::find(2)->perms()->sync(array(5,6,9,10)); //Topdog
		Role::find(3)->perms()->sync(array(1,2,3,4,7,10)); //Watchdog
		Role::find(4)->perms()->sync(array(1,3)); //Underdog
		Role::find(5)->perms()->sync(array()); //Applicant
	}
}
