<?php

class AppRevampMigrationTableSeeder extends Seeder {
	public function run()
	{
		//Old Migration Database
		$olddb = DB::connection('old_lara');
		Eloquent::unguard();
		//Offsets for new ID-ing
		$user_offset = 434;
		$forum_offset = 1;
		$thread_offset = 1;
		$post_offset = 3;
		//Migrate Users
		$oldusers = $olddb->table('users')->get();
		foreach($oldusers as $user) {
			DB::table('users')->insert(array(
				'id' => $user->id - $user_offset,
				'username' => $user->username,
				'email' => $user->email,
				'password' => $user->password,
				'remember_token' => $user->remember_token,
				'blocked' => $user->blocked,
				'created_at' => $user->created_at,
				'updated_at' => $user->updated_at,
				'activation' => $user->activation
			));
		}

		// Migrate Forums
		$oldforums = $olddb->table('forums')->get();
		foreach($oldforums as $forum) {
			DB::table('forums')->insert(array(
				'id' => $forum->id - $forum_offset,
				'name' => $forum->name,
				'description' => $forum->description,
				'created_at' => $forum->created_at,
				'updated_at' => $forum->updated_at
				//Permission IDs will be autoset to either null or 0, permission seeder
				//will create the permissions and assign them to the forums.
			));
		}

		//Migrate Threads
		$oldthreads = $olddb->table('threads')->get();
		foreach($oldthreads as $thread) {
			DB::table('threads')->insert(array(
				'id' => $thread->id - $thread_offset,
				'forum_id' => $thread->forum_id - $forum_offset,
				'user_id' => $thread->user_id -$user_offset,
				'title' => $thread->title,
				'content' => $thread->content,
				'views' => $thread->views,
				'created_at' => $thread->created_at,
				'updated_at' => $thread->updated_at,
				'locked_by_user_id' => 0,
				'edited_by_user_id' => 0
			));
		}

		//Migrate Posts
		$oldposts = $olddb->table('posts')->get();
		foreach($oldposts as $post) {
			DB::table('posts')->insert(array(
				'id' => $post->id - $post_offset,
				'thread_id' => $post->thread_id - $thread_offset,
				'user_id' => $post->user_id - $user_offset,
				'content' => $post->content,
				'created_at' => $post->created_at,
				'updated_at' => $post->updated_at,
				'locked_by_user_id' => 0,
				'edited_by_user_id' => 0
			));
		}

		//Migrate Slides
		$oldslides = $olddb->table('slides')->get();
		foreach($oldslides as $slide) {
			DB::table('slides')->insert(array(
				'id' => $slide->id,
				'url' => $slide->url,
				'name' => $slide->name,
				'title' => $slide->title,
				'description' => $slide->description,
				'created_at' => $slide->created_at,
				'updated_at' => $slide->updated_at,
				'active' => $slide->active,
			));
		}

		//Migrate Transaction Types
		$oldtypes = $olddb->table('transactiontypes')->get();
		foreach($oldtypes as $type) {
			DB::table('transactiontypes')->insert(array(
				'id' => $type->id,
				'name' => $type->name,
				'created_at' => $type->created_at,
				'updated_at' => $type->updated_at,
			));
		}

		//Migrate Transactions
		$oldtransactions = $olddb->table('transactions')->get();
		foreach($oldtransactions as $trans) {
			DB::table('transactions')->insert(array(
				'id' => $trans->id,
				'user_id' => $trans->user_id - $user_offset,
				'transactiontype_id' => $trans->transactiontype_id,
				'gil' => $trans->gil,
				'created_at' => $trans->created_at,
				'updated_at' => $trans->updated_at,
			));
		}

		//Use the NewPermissionsTableSeeder to setup the permissions.
		$permSeeder = new NewPermissionsTableSeeder;
		$permSeeder->run();

		//Migrate User Roles
		$olduserroles = $olddb->table('assigned_roles')->get();
		foreach($olduserroles as $role) {
			DB::table('assigned_roles')->insert(array(
				'user_id' => $role->user_id - $user_offset,
				'role_id' => $role->role_id
			));
		}
		//Create The Roles
		Role::create(array('id' => '1', 'name' => 'Server Admin'));
		Role::create(array('id' => '2', 'name' => 'Topdog'));
		Role::create(array('id' => '3', 'name' => 'Watchdog'));
		Role::create(array('id' => '4', 'name' => 'Underdog'));
		Role::create(array('id' => '5', 'name' => 'Applicant'));
		//Give Roles Some Ideal Permissions
		$admin = Role::findOrFail(1);
		$topdog = Role::findOrFail(2);
		$watchdog = Role::findOrFail(3);
		$underdog = Role::findOrFail(4);
		$applicant = Role::findOrFail(5);

		$admin->perms()->sync(array(1));
		$topdog->perms()->sync(array(1));
		$watchdog->perms()->sync(array(
			11,12,13,14,15,16,17,18,19,20,
			21,22,23,24,25,26,27,28,29,30,
			31,32,33,34,35,36,37,38,39,40,
			41,42,51,53,54,55,56,57,58,59,
			60,61,62,63,64,65,66,67,68,69,
			70,71,72,73,74,75,77,78,79,80,
			81,82,83,84,85,86,87,88,89,90,
			99,100
		));
		$underdog->perms()->sync(array(
			17,18,27,29,30,39,53,54,63,65,
			66,75,77,78,87,89,90,99
		));

	}
}
