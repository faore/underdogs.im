<?php

class PermissionsTest extends TestCase {

  public function testIgnorePermissions() {
    $this->resetdb();
    //Test that all actions can be performed with the ignore Permissions permission.

    $user = User::find(1);
    $forum = Forum::find(1);
    //Assert attachment.
    $this->assertTrue($user->can("IgnorePermissions"));
    //Assert Tests Relating to Forum-Specific Permissions
    $this->assertTrue($user->canViewForum($forum));
    $this->assertTrue($user->canPostInForum($forum));
    $this->assertTrue($user->canLockInForum($forum));
    $this->assertTrue($user->canStickyInForum($forum));
    $this->assertTrue($user->canDeleteInForum($forum));
    $this->assertTrue($user->canMoveInForum($forum));
    $this->assertTrue($user->canEditInForum($forum));
    $this->assertTrue($user->canPostInForum($forum));
    $this->assertTrue($user->canLockOwnInForum($forum));
    $this->assertTrue($user->canDeleteOwnInForum($forum));
    $this->assertTrue($user->canEditOwnInForum($forum));
    $this->assertTrue($user->canMoveOwnInForum($forum));
  }

  public function testNoPermissions() {
    $user = User::find(5);
    $forum = Forum::find(1);
    $this->assertFalse($user->canViewForum($forum));
    $this->assertFalse($user->canPostInForum($forum));
    $this->assertFalse($user->canLockInForum($forum));
    $this->assertFalse($user->canStickyInForum($forum));
    $this->assertFalse($user->canDeleteInForum($forum));
    $this->assertFalse($user->canMoveInForum($forum));
    $this->assertFalse($user->canEditInForum($forum));
    $this->assertFalse($user->canPostInForum($forum));
    $this->assertFalse($user->canLockOwnInForum($forum));
    $this->assertFalse($user->canDeleteOwnInForum($forum));
    $this->assertFalse($user->canEditOwnInForum($forum));
    $this->assertFalse($user->canMoveOwnInForum($forum));
  }

  public function testNoRole() {
    $user = User::find(6);
    $forum = Forum::find(1);
    $this->assertFalse($user->canViewForum($forum));
    $this->assertFalse($user->canPostInForum($forum));
    $this->assertFalse($user->canLockInForum($forum));
    $this->assertFalse($user->canStickyInForum($forum));
    $this->assertFalse($user->canDeleteInForum($forum));
    $this->assertFalse($user->canMoveInForum($forum));
    $this->assertFalse($user->canEditInForum($forum));
    $this->assertFalse($user->canPostInForum($forum));
    $this->assertFalse($user->canLockOwnInForum($forum));
    $this->assertFalse($user->canDeleteOwnInForum($forum));
    $this->assertFalse($user->canEditOwnInForum($forum));
    $this->assertFalse($user->canMoveOwnInForum($forum));
  }

  public function testAssignedPermissions() {
    $user = User::findOrFail(2);
    $forum = Forum::findOrFail(1);
    $this->assertTrue($user->canViewForum($forum));
    $this->assertTrue($user->canPostInForum($forum));
    $this->assertTrue($user->canLockInForum($forum));
    $this->assertTrue($user->canStickyInForum($forum));
    $this->assertTrue($user->canDeleteInForum($forum));
    $this->assertTrue($user->canMoveInForum($forum));
    $this->assertTrue($user->canEditInForum($forum));
    $this->assertTrue($user->canPostInForum($forum));
    $this->assertTrue($user->canLockOwnInForum($forum));
    $this->assertTrue($user->canDeleteOwnInForum($forum));
    $this->assertTrue($user->canEditOwnInForum($forum));
    $this->assertTrue($user->canMoveOwnInForum($forum));
  }

  public function testEditThreadLogic() {
    $admin = User::findOrFail(1);
    $watchdog = User::findOrFail(3);
    $underdog = User::findOrFail(4);
    $applicant = User::findOrFail(5);

    $adminThread = Thread::findOrFail(1);
    $topdogThread = Thread::findOrFail(2);
    $watchdogThread = Thread::findOrFail(3);
    $underdogThread = Thread::findOrFail(4);
    $applicantThread = Thread::findOrFail(5);
    //Ignore Flag (Allowed)
    $this->assertTrue($admin->canEditThread($underdogThread)); //Admin Editing Admin
    //Own (Allowed)
    $this->assertTrue($underdog->canEditThread($underdogThread)); //Underdog Editing Own
    //Own (Not Allowed)
    $this->assertFalse($applicant->canEditThread($applicantThread)); //Applicant Editing Own
    //Other user without perm in forum or perm over role (Not Allowed)
    $this->assertFalse($underdog->canEditThread($watchdogThread)); //Underdog Editing Watchdog
    //Other user with perm in forum but not perm over role (Not Allowed)
    $this->assertFalse($watchdog->canEditThread($topdogThread));
    //Other user with perm in forum and perm over role (Allowed)
    $this->assertTrue($watchdog->canEditThread($underdogThread));
  }

  public function testDeleteThreadLogic() {
    $admin = User::findOrFail(1);
    $watchdog = User::findOrFail(3);
    $underdog = User::findOrFail(4);
    $applicant = User::findOrFail(5);

    $adminThread = Thread::findOrFail(1);
    $topdogThread = Thread::findOrFail(2);
    $watchdogThread = Thread::findOrFail(3);
    $underdogThread = Thread::findOrFail(4);
    $applicantThread = Thread::findOrFail(5);
    //Ignore Flag (Allowed)
    $this->assertTrue($admin->canDeleteThread($underdogThread));
    //Own (Allowed)
    $this->assertTrue($underdog->canDeleteThread($underdogThread));
    //Own (Not Allowed)
    $this->assertFalse($applicant->canDeleteThread($applicantThread));
    //Other user without perm in forum or perm over role (Not Allowed)
    $this->assertFalse($underdog->canDeleteThread($watchdogThread));
    //Other user with perm in forum but not perm over role (Not Allowed)
    $this->assertFalse($watchdog->canDeleteThread($topdogThread));
    //Other user with perm in forum and perm over role (Allowed)
    $this->assertTrue($watchdog->canDeleteThread($underdogThread));
  }

  public function testLockThreadLogic() {
    $admin = User::findOrFail(1);
    $watchdog = User::findOrFail(3);
    $underdog = User::findOrFail(4);
    $applicant = User::findOrFail(5);

    $adminThread = Thread::findOrFail(1);
    $topdogThread = Thread::findOrFail(2);
    $watchdogThread = Thread::findOrFail(3);
    $underdogThread = Thread::findOrFail(4);
    $applicantThread = Thread::findOrFail(5);
    //Ignore Flag (Allowed)
    $this->assertTrue($admin->canLockThread($underdogThread));
    //Own (Allowed)
    $this->assertTrue($underdog->canLockThread($underdogThread));
    //Own (Not Allowed)
    $this->assertFalse($applicant->canLockThread($applicantThread));
    //Other user without perm in forum or perm over role (Not Allowed)
    $this->assertFalse($underdog->canLockThread($watchdogThread));
    //Other user with perm in forum but not perm over role (Not Allowed)
    $this->assertFalse($watchdog->canLockThread($topdogThread));
    //Other user with perm in forum and perm over role (Allowed)
    $this->assertTrue($watchdog->canLockThread($underdogThread));
  }

  public function testEditPostLogic() {
    $admin = User::findOrFail(1);
    $watchdog = User::findOrFail(3);
    $underdog = User::findOrFail(4);
    $applicant = User::findOrFail(5);

    $adminPost = Post::findOrFail(1);
    $topdogPost = Post::findOrFail(2);
    $watchdogPost = Post::findOrFail(3);
    $underdogPost = Post::findOrFail(4);
    $applicantPost = Post::findOrFail(5);
    //Ignore Flag (Allowed)
    $this->assertTrue($admin->canEditPost($underdogPost));
    //Own (Allowed)
    $this->assertTrue($underdog->canEditPost($underdogPost));
    //Own (Not Allowed)
    $this->assertFalse($applicant->canEditPost($applicantPost));
    //Other user without perm in forum or perm over role (Not Allowed)
    $this->assertFalse($underdog->canEditPost($watchdogPost));
    //Other user with perm in forum but not perm over role (Not Allowed)
    $this->assertFalse($watchdog->canEditPost($topdogPost));
    //Other user with perm in forum and perm over role (Allowed)
    $this->assertTrue($watchdog->canEditPost($underdogPost));
  }

  public function testDeletePostLogic() {
    $admin = User::findOrFail(1);
    $watchdog = User::findOrFail(3);
    $underdog = User::findOrFail(4);
    $applicant = User::findOrFail(5);

    $adminPost = Post::findOrFail(1);
    $topdogPost = Post::findOrFail(2);
    $watchdogPost = Post::findOrFail(3);
    $underdogPost = Post::findOrFail(4);
    $applicantPost = Post::findOrFail(5);
    //Ignore Flag (Allowed)
    $this->assertTrue($admin->canDeletePost($underdogPost));
    //Own (Allowed)
    $this->assertTrue($underdog->canDeletePost($underdogPost));
    //Own (Not Allowed)
    $this->assertFalse($applicant->canDeletePost($applicantPost));
    //Other user without perm in forum or perm over role (Not Allowed)
    $this->assertFalse($underdog->canDeletePost($watchdogPost));
    //Other user with perm in forum but not perm over role (Not Allowed)
    $this->assertFalse($watchdog->canDeletePost($topdogPost));
    //Other user with perm in forum and perm over role (Allowed)
    $this->assertTrue($watchdog->canDeletePost($underdogPost));
  }

  public function testLockPostLogic() {
    $admin = User::findOrFail(1);
    $watchdog = User::findOrFail(3);
    $underdog = User::findOrFail(4);
    $applicant = User::findOrFail(5);

    $adminPost = Post::findOrFail(1);
    $topdogPost = Post::findOrFail(2);
    $watchdogPost = Post::findOrFail(3);
    $underdogPost = Post::findOrFail(4);
    $applicantPost = Post::findOrFail(5);
    //Ignore Flag (Allowed)
    $this->assertTrue($admin->canLockPost($underdogPost));
    //Own (Allowed)
    $this->assertTrue($underdog->canLockPost($underdogPost));
    //Own (Not Allowed)
    $this->assertFalse($applicant->canLockPost($applicantPost));
    //Other user without perm in forum or perm over role (Not Allowed)
    $this->assertFalse($underdog->canLockPost($watchdogPost));
    //Other user with perm in forum but not perm over role (Not Allowed)
    $this->assertFalse($watchdog->canLockPost($topdogPost));
    //Other user with perm in forum and perm over role (Allowed)
    $this->assertTrue($watchdog->canLockPost($underdogPost));
  }

  public function testPostInThreadLogic() {
    $admin = User::findOrFail(1);
    $topdog = User::findOrFail(2);
    $watchdog = User::findOrFail(3);
    $underdog = User::findOrFail(4);
    $applicant = User::findOrFail(5);

    $adminThread = Thread::findOrFail(1);
    $topdogThread = Thread::findOrFail(2);
    $watchdogThread = Thread::findOrFail(3);
    $underdogThread = Thread::findOrFail(4);
    $applicantThread = Thread::findOrFail(5);

    $lockedThread = Thread::findOrFail(7);

    //Ignore Flag (Allowed)
    $this->assertTrue($admin->canPostInThread($underdogThread));
    //Ignore Flag Locked (Allowed)
    $this->assertTrue($admin->canPostInThread($lockedThread));
    //Locked No Perm (Not Allowed)
    $this->assertFalse($underdog->canPostInThread($lockedThread));
    //Locked With Perm (Allowed)
    $this->assertTrue($topdog->canPostInThread($lockedThread));
    //Unlocked With Perm (Allowed)
    $this->assertTrue($underdog->canPostInThread($watchdogThread));
    //Unlocked Without Perm (Not Allowed)
    $this->assertFalse($applicant->canPostInThread($underdogThread));
  }

  protected function resetdb() {
    //Artisan::call('migrate:install');
    Artisan::call('migrate:refresh');
    $seeder = New CreateTestingEnvironmentTableSeeder;
    $seeder->run();
  }
}
