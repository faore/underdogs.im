<?php

class ConvertPostsTest extends TestCase {

	public function setUp() {
		parent::setUp();
		$this->converter = new ConvertPosts();
	}

	public function testNoTags()
	{
		$this->assertEquals("foo\nbar", $this->converter->updatePost("foo\nbar"));
	}

	public function testBold()
	{
		$this->assertEquals("**foo**", $this->converter->updatePost("[b]foo[/b]"));
		$this->assertEquals("**foo**", $this->converter->updatePost("[bold]foo[/bold]"));
	}

	public function testUnderline()
	{
		$this->assertEquals("*foo*", $this->converter->updatePost("[u]foo[/u]"));
		$this->assertEquals("*foo*", $this->converter->updatePost("[underline]foo[/underline]"));
	}

	public function testStrikeThrough()
	{
		$this->assertEquals("~~foo~~", $this->converter->updatePost("[s]foo[/s]"));
		$this->assertEquals("~~foo~~", $this->converter->updatePost("[strike]foo[/strike]"));
	}

	public function testImages()
	{
		$this->assertEquals("![](foo)", $this->converter->updatePost("[img]foo[/img]"));
	}

	public function testColorTags()
	{
		$this->assertEquals("foo", $this->converter->updatePost("[color=red]foo[/color]"));
		$this->assertEquals("foo", $this->converter->updatePost("[color=fuck]foo[/color]"));
		$this->assertEquals("foo", $this->converter->updatePost("[color=this]foo[/color]"));
		$this->assertEquals("foo", $this->converter->updatePost("[color=\"shit\"]foo[/color]"));
	}

	public function testLinks()
	{
		$this->assertEquals("[foo](bar)", $this->converter->updatePost("[url=bar]foo[/url]"));
	}

	public function testLists()
	{
		$this->assertEquals("\n* foo\n* bar\nbaz", $this->converter->updatePost("[list]\n[*]foo\n[*]bar\nbaz[/list]"));
		$this->assertEquals("\n* foo\n* bar\nbaz", $this->converter->updatePost("[list=a]\n[*]foo\n[*]bar\nbaz[/list]"));
		$this->assertEquals("\n* foo\n* bar\nbaz", $this->converter->updatePost("[list=B]\n[*]foo\n[*]bar\nbaz[/list]"));
	}

	public function testCenterTags()
	{
		$this->assertEquals("foo", $this->converter->updatePost("[center]foo[/center]"));
		$this->assertEquals("**foo**", $this->converter->updatePost("[center][b]foo[/b][/center]"));
	}

	public function testQuotes()
	{
		$this->assertEquals("> foo",
			$this->converter->updatePost("[quote=\"Timlst6\" post=533]foo[/quote]"));

		$this->assertEquals("> foo\n> bar",
			$this->converter->updatePost("[quote=\"Timlst6\" post=533]foo\nbar[/quote]"));
	}

	public function testAttachments()
	{
		$this->assertEquals("Amazing mistell:\n\n",
			$this->converter->updatePost("Amazing mistell:\n\n[attachment=2]mistell_funny.png[/attachment]"));
	}

	public function testMultiLine()
	{
		$this->assertEquals("**foo\nbar**", $this->converter->updatePost("[b]foo\nbar[/b]"));
	}

	public function testUpperCase() {
		$this->assertEquals("![](http://i.imgur.com/SnIOdvx.jpg)",
			$this->converter->updatePost("[IMG]http://i.imgur.com/SnIOdvx.jpg[/IMG]"));
	}

	public function testMultipleImages() {
		$this->assertEquals("So - we where in Hakkue Mannor once - and this happened:

												![](http://i.imgur.com/NsoH5hGl.png)

												![](http://i.imgur.com/BG46bK6l.png)

												![](http://i.imgur.com/RAfWScbl.png)

												![](http://i.imgur.com/FlZQWuil.png)

												The boss and none of the aoe marking on the floor appeared the entire battle -.-
												Somehow we all lived though! xD",

			$this->converter->updatePost("So - we where in Hakkue Mannor once - and this happened:

												[IMG]http://i.imgur.com/NsoH5hGl.png[/IMG]

												[IMG]http://i.imgur.com/BG46bK6l.png[/IMG]

												[IMG]http://i.imgur.com/RAfWScbl.png[/IMG]

												[IMG]http://i.imgur.com/FlZQWuil.png[/IMG]

												The boss and none of the aoe marking on the floor appeared the entire battle -.-
												Somehow we all lived though! xD"));
	}

}
