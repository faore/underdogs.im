<?php

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
  public function editThisPosts() {
    return $this->belongsTo('Permission', 'edit_this_posts_permission_id');
  }

  public function deleteThisPosts() {
    return $this->belongsTo('Permission', 'delete_this_posts_permission_id');
  }

  public function lockThisPosts() {
    return $this->belongsTo('Permission', 'lock_this_posts_permission_id');
  }
}
