<?php
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Zizaco\Entrust\HasRole;

class User extends Eloquent implements UserInterface, RemindableInterface {
	use HasRole;

	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'users';

	/**
	* The attributes excluded from the model's JSON form.
	*
	* @var array
	*/
	protected $hidden = array('password');

	/**
	* Get the unique identifier for the user.
	*
	* @return mixed
	*/
	public function getAuthIdentifier()
	{
	return $this->getKey();
	}

	/**
	* Get the password for the user.
	*
	* @return string
	*/
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	*	Get the token used to remember a user's session.
	*
	*	@return string
	*/
	public function getRememberToken() {
		return $this->remember_token;
	}

	/**
	*	Sets the value of the user's remember token.
	*
	*	@return void
	*/
	public function setRememberToken($value) {
		$this->remember_token = $value;
	}

	/**
	*	Returns the name of the field for remember tokens.
	*
	* @return string 'remember_token'
	*/
	public function getRememberTokenName() {
		return 'remember_token';
	}

	/**
	* Get the e-mail address where password reminders are sent.
	*
	* @return string
	*/
	public function getReminderEmail()
	{
		return $this->email;
	}

	/**
	*	Returns the HTML to produce all the rank icons of a user.
	*
	*	@return string
	*/
	public function getRankIcons() {
		$return = '';
		foreach($this->roles()->get() as $role) {
			switch($role->name) {
				case 'Topdog':
					$return = $return . '<img class="ud-rank" src="/img/rank-ico/td-ico.png" alt="TD" data-uk-tooltip title="Topdog">';
					break;
				case 'Watchdog':
					$return = $return . '<img class="ud-rank" src="/img/rank-ico/wd-ico.png" alt="WD" data-uk-tooltip title="Watchdog">';
					break;
				case 'Underdog':
					$return = $return . '<img class="ud-rank" src="/img/rank-ico/ud-ico.png" alt="UD" data-uk-tooltip title="Underdog">';
					break;
				case 'Server Admin':
					$return = $return . '<img class="ud-rank" src="/img/rank-ico/sa-ico.png" alt="SA" data-uk-tooltip title="Server Admin">';
					break;
				default:
					break;
			}
		}
		return $return;
	}

	public function threads() {
		return $this->hasMany('Thread');
	}

	public function posts() {
		return $this->hasMany('Post');
	}

	public function shouts() {
		return $this->hasMany('Shout');
	}

	public function Gardenreservations() {
		return $this->hasMany('Gardenreservation');
	}

	/**
	*	Returns extra class names to highlight a post based on rank.
	*
	*	@return string
	*/
	public function getRankPostColors() {
		$return = '';
		foreach($this->roles()->get() as $role) {
			switch($role->name) {
				case 'Topdog':
					$return = $return . ' ud-post-td ';
					break;
				case 'Watchdog':
					$return = $return . ' ud-post-wd ';
					break;
				case 'Underdog':
					$return = $return . ' ud-post-ud';
					break;
				default:
					break;
			}
		}
		return $return;
	}

	/**
	*	Returns the URL of the user's profile.
	*
	*	@return string
	*/
	public function profileUrl() {
		$url = '/users/' . $this->id;
		return $url;
	}

	/**
	*	Returns the gravatar image URL for the user.
	*
	* @param int $size Size, in pixels for the avatar to be fetched.
	* @return string
	*/
	public function gravatarUrl($size = 40) {
		$url = '//www.gravatar.com/avatar/' . md5(strtolower($this->email)) . '?s=' . $size;
		return $url;
	}

	public function transactions() {
		$this->hasMany('Transaction');
	}

	// Forum Permission Checks
	public function canViewForum($forum) {
		if($this->can($forum->viewPermission()->first()->name) || $this->can('IgnorePermissions')) {
			return true;
		}
		return false;
	}

	public function canPostInForum($forum) {
		if($this->can($forum->postPermission()->first()->name) || $this->can('IgnorePermissions')) {
			return true;
		}
		return false;
	}

	public function canLockInForum($forum) {
		if($this->can($forum->lockPermission()->first()->name) || $this->can('IgnorePermissions')) {
			return true;
		}
		return false;
	}

	public function canStickyInForum($forum) {
		if($this->can($forum->stickyPermission()->first()->name) || $this->can('IgnorePermissions')) {
			return true;
		}
		return false;
	}

	public function canDeleteInForum($forum) {
		if($this->can($forum->deletePermission()->first()->name) || $this->can('IgnorePermissions')) {
			return true;
		}
		return false;
	}

	public function canMoveInForum($forum) {
		if($this->can($forum->movePermission()->first()->name) || $this->can('IgnorePermissions')) {
			return true;
		}
		return false;
	}

	public function canEditInForum($forum) {
		if($this->can($forum->viewPermission()->first()->name) || $this->can('IgnorePermissions')) {
			return true;
		}
		return false;
	}

	public function canPostInLockedInForum($forum) {
		if($this->can($forum->postInLockedPermission()->first()->name) || $this->can('IgnorePermissions')) {
			return true;
		}
		return false;
	}

	public function canLockOwnInForum($forum) {
		if($this->can($forum->lockOwnPermission()->first()->name) || $this->can('IgnorePermissions')) {
			return true;
		}
		return false;
	}

	public function canDeleteOwnInForum($forum) {
		if($this->can($forum->deleteOwnPermission()->first()->name) || $this->can('IgnorePermissions')) {
			return true;
		}
		return false;
	}

	public function canEditOwnInForum($forum) {
		if($this->can($forum->editOwnPermission()->first()->name) || $this->can('IgnorePermissions')) {
			return true;
		}
		return false;
	}

	public function canMoveOwnInForum($forum) {
		if($this->can($forum->moveOwnPermission()->first()->name) || $this->can('IgnorePermissions')) {
			return true;
		}
		return false;
	}

	//Permissions for Forum on another user.
	//Threads
	public function canEditThread($thread) {
		$user = $thread->user()->first();
		$roles = $user->roles()->get();
		$forum = $thread->forum()->first();
		if($user->id == $this->id && $this->canEditOwnInForum($forum)) {
			return true;
		}
		foreach($roles as $role) {
			if(!($this->can($role->editThisPosts()->first()->name) && $this->canEditInForum($forum)) && !$this->can('IgnorePermissions')) {
				return false;
			}
		}
		return true;
	}

	public function canDeleteThread($thread) {
		$user = $thread->user()->first();
		$roles = $user->roles()->get();
		$forum = $thread->forum()->first();
		if($user->id == $this->id && $this->canDeleteOwnInForum($forum)) {
			return true;
		}
		foreach($roles as $role) {
			if(!($this->can($role->deleteThisPosts()->first()->name) && $this->canDeleteInForum($forum)) && !$this->can('IgnorePermissions')) {
				return false;
			}
		}
		return true;
	}

	public function canLockThread($thread) {
		$user = $thread->user()->first();
		$roles = $user->roles()->get();
		$forum = $thread->forum()->first();
		if($user->id == $this->id && $this->canLockOwnInForum($forum)) {
			return true;
		}
		foreach($roles as $role) {
			if(!($this->can($role->lockThisPosts()->first()->name) && $this->canLockInForum($forum)) && !$this->can('IgnorePermissions')) {
				return false;
			}
		}
		return true;
	}

	//Posts
	public function canEditPost($post) {
		$user = $post->user()->first();
		$roles = $user->roles()->get();
		$forum = $post->thread()->first()->forum()->first();
		if($user->id == $this->id && $this->canEditOwnInForum($forum)) {
			return true;
		}
		foreach($roles as $role) {
			if(!($this->can($role->editThisPosts()->first()->name) && $this->canEditInForum($forum)) && !$this->can('IgnorePermissions')) {
				return false;
			}
		}
		return true;
	}

	public function canDeletePost($post) {
		$user = $post->user()->first();
		$roles = $user->roles()->get();
		$forum = $post->thread()->first()->forum()->first();
		if($user->id == $this->id && $this->canDeleteOwnInForum($forum)) {
			return true;
		}
		foreach($roles as $role) {
			if(!($this->can($role->deleteThisPosts()->first()->name) && $this->canDeleteInForum($forum)) && !$this->can('IgnorePermissions')) {
				return false;
			}
		}
		return true;
	}

	public function canLockPost($post) {
		$user = $post->user()->first();
		$roles = $user->roles()->get();
		$forum = $post->thread()->first()->forum()->first();
		if($user->id == $this->id && $this->canLockOwnInForum($forum)) {
			return true;
		}
		foreach($roles as $role) {
			if(!($this->can($role->lockThisPosts()->first()->name) && $this->canLockInForum($forum)) && !$this->can('IgnorePermissions')) {
				return false;
			}
		}
		return true;
	}

	public function canPostInThread($thread) {
		if($thread->lockedBy) {
			if($this->canPostInLockedInForum($thread->forum()->first()) && $this->canPostInForum($thread->forum()->first())) {
				return true;
			}
		} else {
			if($this->canPostInForum($thread->forum()->first())) {
				return true;
			}
		}
		return false;
	}
}
