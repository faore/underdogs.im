<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class Forum extends \Eloquent {
	use SoftDeletingTrait;
	protected $fillable = [];
	
	public function threads() {
		return $this->hasMany('Thread');
	}

	// Permissions

	public function viewPermission() {
		return $this->belongsTo('Permission', 'view_permission_id');
	}

	public function postPermission() {
		return $this->belongsTo('Permission', 'post_permission_id');
	}

	public function lockPermission() {
		return $this->belongsTo('Permission', 'lock_permission_id');
	}

	public function stickyPermission() {
		return $this->belongsTo('Permission', 'sticky_permission_id');
	}

	public function deletePermission() {
		return $this->belongsTo('Permission', 'delete_permission_id');
	}

	public function movePermission() {
		return $this->belongsTo('Permission', 'move_permission_id');
	}

	public function editPermission() {
		return $this->belongsTo('Permission', 'edit_permission_id');
	}

	public function postInLockedPermission() {
		return $this->belongsTo('Permission', 'post_in_locked_permission_id');
	}

	public function lockOwnPermission() {
		return $this->belongsTo('Permission', 'lock_own_permission_id');
	}

	public function deleteOwnPermission() {
		return $this->belongsTo('Permission', 'delete_own_permission_id');
	}

	public function editOwnPermission() {
		return $this->belongsTo('Permission', 'edit_own_permission_id');
	}

	public function moveOwnPermission() {
		return $this->belongsTo('Permission', 'move_own_permission_id');
	}
}
