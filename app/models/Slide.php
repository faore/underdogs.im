<?php

class Slide extends \Eloquent {
	protected $fillable = [];

	public static function createSlides() {
		$slidedata = '';
		foreach(Slide::where('active', '=', '1')->get() as $slide) {
			$slidedata = $slidedata . '<img src="' . $slide->url . '" alt="' . $slide->title . '">';
		}
		return $slidedata;
	}
}
