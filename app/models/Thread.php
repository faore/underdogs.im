<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class Thread extends \Eloquent {
	use SoftDeletingTrait;
	protected $fillable = [];

	public function forum() {
		return $this->belongsTo('Forum');
	}
	public function posts() {
		return $this->hasMany('Post');
	}
	public function user() {
		return $this->belongsTo('User');
	}
	public function lockedBy() {
		return $this->belongsTo('User', 'locked_by_user_id');
	}
	public function editedBy() {
		return $this->belongsTo('User', 'edited_by_user_id');
	}
	/**
	*	Creates a full url with a leading slash for the instance of this model.
	*
	* @return string
	*/
	public function url() {
		$url =  '/community/forum/' . $this->forum_id . '/thread/' . $this->id;
		return $url;
	}

	public function view() {
		$this->timestamps = false;
		$this->views++;
		$this->save();
		$this->timestamps = true;
	}
}
