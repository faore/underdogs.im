<?php
use \Carbon;

class Transaction extends \Eloquent {
	protected $fillable = [];
	public function user() {
		return $this->belongsTo('User');
	}
	public function transactiontype() {
		return $this->belongsTo('Transactiontype');
	}

	public static function getStackedAreaData($begin = false, $end = false) {
		//See what data period we're fetching.
		if(!$begin || !$end) {
			$transactions = Transaction::all();
		} else {
			$transactions = Transaction::whereRaw('created_at >= ? and created_at <= ?', array($begin, $end))->orderBy('created_at','asc')->get();
		}
		$axisNames = array('Day');
		$presentUserIds = array();
		//Get all the users in the dataset.
		foreach($transactions as $transaction) {
			if(!in_array($transaction->user_id, $presentUserIds)) {
				$presentUserIds[] = $transaction->user_id;
			}
		}
		//For each user in the dataset add, their username to the data.
		foreach($presentUserIds as $current) {
			$axisNames[] = ucfirst(User::find($current)->username);
		}
		$firstTrans = null;
		$lastTrans = null;
		//Find the times of the first and last transaction
		foreach($transactions as $transaction) {
			if($firstTrans > $transaction->created_at || $firstTrans === null) {
				$firstTrans = $transaction->created_at;
			}

			if($lastTrans < $transaction->created_at || $lastTrans === null) {
				$lastTrans = $transaction->created_at;
			}
		}
		//Get rid of time information from first and last transaction, bumping up the day of the last transaction.
		$firstTrans->hour = 0;
		$firstTrans->minute = 0;
		$firstTrans->second = 0;
		$lastTrans->day++;
		$lastTrans->hour = 0;
		$lastTrans->minute = 0;
		$lastTrans->second = 0;

		$datecheck = $firstTrans->copy();
		$outputdata = array();
		$i = 0;
		while($datecheck < $lastTrans) {
			$tmp = array(
				$datecheck->toDateString()
			);
			foreach($presentUserIds as $user) {
				if($i == 0) {
					$tmp[$user] = 0;
				} else {
					$tmp[$user] = $outputdata[$i-1][$user];
				}
				foreach($transactions as $transaction) {
					if($transaction->created_at >= $datecheck && $transaction->created_at < $datecheck->copy()->addDay() && $transaction->user_id == $user) {
						$tmp[$user] = $tmp[$user] + $transaction->gil;
					}
				}
			}
			$outputdata[] = $tmp;
			$datecheck->day++;
			$i++;
		}
		$return = array($axisNames, $outputdata);
		return $return;
	}
}
