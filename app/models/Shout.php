<?php

class Shout extends \Eloquent {
	protected $fillable = [];

	public function user() {
		$this->belongsTo('User');
	}
}