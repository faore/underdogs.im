<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class Post extends \Eloquent {
	use SoftDeletingTrait;
	protected $fillable = [];

	public function thread() {
		return $this->belongsTo('Thread');
	}
	public function user() {
		return $this->belongsTo('User');
	}
}
