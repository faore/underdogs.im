@extends('layout')
@section('content')
<!--Main Content-->
<div class="ud-mainpage-bg uk-container uk-container-center">
  <div class="uk-grid ud-mainpage">
    <div class="uk-width-large-3-4 uk-width-medium-3-4">
      <form action="/users/login" class="uk-form uk-form-horizontal" id="loginpage" method="POST">
        <fieldset>
          <legend>
            Login with Your UnderdogsID
          </legend>
            @if(Input::get('needlogin'))
              <div class="needlogin uk-alert uk-alert-warning" data-uk-alert>
                <a class="uk-alert-close uk-close"></a>
                You need to login to view this page.
              </div>
            @endif
            @if(Input::get('e') == 1)
              <div class="wrongpass uk-alert uk-alert-warning" data-uk-alert>
                <a class="uk-alert-close uk-close"></a>
                The UnderdogsID and password you entered are incorrect.
              </div>
            @elseif(Input::get('e') == 2)
              <div class="banned uk-alert uk-alert-danger uk-alert-large" data-uk-alert>
                <a class="uk-alert-close uk-close"></a>
                <h2>
                  Oh no...We Have Just a Little Problem
                </h2>
                <p>
                  Unfortunately, we can't let you in. It seems you've been banned. If you need to know more about your ban,
                  <a href="#">
                    click here
                  </a>
                  or talk to an officer.
                </p>
                <p>
                  Please keep in mind that attempting to circumvent forum security, or trolling our forums will not help your case. Please do not reapply for the guild to dispute a ban. Talk with an officer instead. If you need a copy of the guild rules, you can find them
                  <a href="#">
                    here
                  </a>
                  for your reference. We appreciate your time with the guild and wish you luck in your future endevours.
                </p>
                <p>
                  \-- The Underdogs Guild
                </p>
              </div>
            @elseif(Input::get('e') == 3)
              <div class="wrongpass uk-alert uk-alert-warning" data-uk-alert>
                <a class="uk-alert-close uk-close"></a>
                Your account hasn't been activated, check your email for an activation link.
              </div>
            @endif
          <div class="uk-form-row">
            <label class="uk-form-label">
              UnderdogsID
            </label>
            <div class="uk-form-controls">
              {{ Form::text('username', null, array('placeholder' => 'UnderdogsID')) }}
            </div>
          </div>
          <div class="uk-form-row">
            <label class="uk-form-label">
              Password
            </label>
            <div class="uk-form-controls">
              <input name="password" placeholder="Password" type="password"></input>
            </div>
          </div>
          <div class="uk-form-row">
            <label class="uk-form-label">
              Remember You?
            </label>
            <div class="uk-form-controls">
              <input type="hidden" name="_token" value="{{ csrf_token(); }}">
              <input name="remember" type="checkbox">
                Remember Me
              </input>
            </div>
          </div>
          <div class="uk-form-row">
            <div class="uk-form-controls">
              <button class="uk-button uk-button-primary" name="action" type="submit" value="login">
                Login!
              </button>
              <a class="uk-button uk-button-success" href="/users/create">
                Join Us!
              </a>
            </div>
          </div>
          <div class="uk-form-row">
            <div class="uk-form-controls">
              <a href="#">
                Forget your password?
              </a>
            </div>
          </div>
        </fieldset>
      </form>
    </div>
    <div class="uk-width-1-4 uk-hidden-small">
      <h2>
        We're Recruiting!
      </h2>
      <p>
        Minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam.
        <i class="uk-icon uk-icon-heart"></i>
      </p>
    </div>
  </div>
</div>
<!--End Main Content-->
@stop
