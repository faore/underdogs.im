@extends('layout')
@section('content')
<div class="ud-container-pal4">
  <div class="ud-pagetitle uk-container uk-container-center">
    <h1 class="ud-pagetitle">
      Become an Underdog!
    </h1>
  </div>
</div>
<!--Main Content-->
<div class="ud-mainpage-bg uk-container uk-container-center">
  <div class="uk-grid ud-mainpage">
    <div class="uk-width-large-3-4 uk-width-medium-3-4">
    <h2></h2>
      <form action="/users/create" class="uk-form uk-form-horizontal" id="loginpage" method="POST">
        <fieldset>
          <legend>
            Create Your UnderdogsID
          </legend>
          <div class="uk-form-row">
            <label class="uk-form-label">
              Email
            </label>
            <div class="uk-form-controls">
              {{ Form::text('email', null, array('placeholder' => 'me@someplace.derp')) }}
            </div>
          </div>
          <div class="uk-form-row">
            <label class="uk-form-label">
              UnderdogsID
            </label>
            <div class="uk-form-controls">
              {{ Form::text('username', null, array('placeholder' => 'UnderdogsID')) }}
            </div>
          </div>
          <div class="uk-form-row">
            <label class="uk-form-label">
              Password
            </label>
            <div class="uk-form-controls">
              <input name="password" placeholder="Password" type="password">
            </div>
          </div>
          <div class="uk-form-row">
            <label class="uk-form-label">
              Confirm Password
            </label>
            <div class="uk-form-controls">
              <input name="password_confirm" placeholder="Password" type="password">
            </div>
          </div>
          <div class="uk-form-row">
            <label class="uk-form-label">
              Our FC Lead is Myra, what is her name?
            </label>
            <div class="uk-form-controls">
              {{ Form::text('bot_check', null, array('placeholder' => 'Hint: Myra')) }}
            </div>
          </div>
        </fieldset>
        <fieldset>
          <legend>
            Your Application
          </legend>
          <div class="uk-form-row">
            <label class="uk-form-label">
              Character Name
            </label>
            <div class="uk-form-controls">
              {{ Form::text('character_name', null, array('placeholder' => 'Doom Vondoomage')) }}
            </div>
          </div>
          <div class="uk-form-row">
            <label class="uk-form-label">
              Main Class Played
            </label>
            <div class="uk-form-controls">
              {{ Form::text('class', null, array('placeholder' => 'White Lancer')) }}
            </div>
          </div>
          <div class="uk-form-row">
            <label class="uk-form-label">
              Age
            </label>
            <div class="uk-form-controls">
              {{ Form::text('age', null, array('placeholder' => '16545')) }}
            </div>
          </div>
          <div class="uk-form-row">
            <label class="uk-form-label">
              Past Guild Experience
            </label>
            <div class="uk-form-controls">
              {{ Form::textarea('past_guild_experience', null, array('placeholder' => 'I used to be in a pony guild.', 'rows' => '5', 'cols' => '40')) }}
            </div>
          </div>
          <div class="uk-form-row">
            <label class="uk-form-label">
              Describe Yourself in Five Words
            </label>
            <div class="uk-form-controls">
              {{ Form::textarea('five_words', null, array('placeholder' => 'Random, imaginative, emotional, crazy, sleepy?', 'rows' => '5', 'cols' => '40')) }}
            </div>
          </div>
          <div class="uk-form-row">
            <label class="uk-form-label">
              Anything Else You Want to Tell Us?
            </label>
            <div class="uk-form-controls">
              {{ Form::textarea('anything_else', null, array('placeholder' => 'I ate donuts for breakfasts.', 'rows' => '5', 'cols' => '40')) }}
            </div>
          </div>
          <div class="uk-form-row">
            <label class="uk-form-label">
              Where did you hear of us?
            </label>
            <div class="uk-form-controls">
              {{ Form::textarea('where_did_you_hear_of_us', null, array('placeholder' => 'FFXIV Forums', 'rows' => '5', 'cols' => '40')) }}
            </div>
          </div>
        </fieldset>
        <div class="uk-form-row">
          <div class="uk-form-controls">
            <input type="hidden" name="_token" value="{{ csrf_token(); }}">
            <button class="uk-button uk-button-primary" name="action" type="submit" value="create">
              Apply
            </button>
          </div>
        </div>
      </form>
    </div>
    <div class="uk-width-1-4 uk-hidden-small">
      <h2>
        Questions?
      </h2>
      <p>
        No worries! Either ask the questions in your application and we'll get back to you or get in touch with Faore Hanato, Mirakell Wilo or Remeliez Draconflair.
        <i class="uk-icon uk-icon-heart"></i>
      </p>
    </div>
  </div>
</div>
<!--End Main Content-->
@stop
