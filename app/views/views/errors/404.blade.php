@extends('errors')
@section('content')
<!--Main Content-->
<div class="uk-vertical-align uk-text-center">
  <div class="uk-vertical-align-middle ud-error-container ud-container-center uk-grid">
    <div class="uk-width-1-3 uk-hidden-small ud-sadface">
      ?
    </div>
    <div class="uk-width-large-2-3 uk-width-medium-2-3 ud-error-info uk-width-small-1-1 ud-mobile-padding">
      <h1 class="ud-error-text">
        Where is Page?
      </h1>
      <p class="ud-error-text">
        We ran into a problem. We can't find whatever it is you're looking for. Chances are you're looking for things like unicorns. And everyone knows they don't exist.
      </p>
      <a class="uk-button uk-button-danger" href="/">
        Run Home!
      </a>
      <p>
        If you believe this page should be here and somethings broken, tell our tech monkeys you got a 404 at {{ Request::url() }}.
      </p>
    </div>
  </div>
</div>
<!--End Main Content-->
@stop