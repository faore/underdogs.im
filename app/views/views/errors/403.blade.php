@extends('errors')
@section('content')
<!--Main Content-->
<div class="uk-vertical-align uk-text-center">
  <div class="uk-vertical-align-middle ud-error-container ud-container-center uk-grid">
    <div class="uk-width-1-3 uk-hidden-small ud-errorlock">
      <i class="uk-icon-lock"></i>
    </div>
    <div class="uk-width-large-2-3 uk-width-medium-2-3 ud-error-info uk-width-small-1-1 ud-mobile-padding">
      <h1 class="ud-error-text">
        That's a nono!
      </h1>
      <p class="ud-error-text">
        It looks like you're trying to get somewhere you aren't suppossed to be in. Shoo!~
      </p>
      <a class="uk-button uk-button-danger" href="/">
        Run Home!
      </a>
      </a>
      <p>
        If you believe this page should be here and somethings broken, tell our tech monkeys you got a 403 at {{ Request::url() }}.
      </p>
    </div>
  </div>
</div>
<!--End Main Content-->
@stop