@extends('layout')
@section('content')
<div class="ud-container-pal4">
  <div class="ud-pagetitle uk-container uk-container-center">
    <h1 class="ud-pagetitle">
      Forum
    </h1>
  </div>
</div>
<div class="ud-container-wht">
  <div class="ud-mainpage-bg uk-container uk-container-center">
    {{ Breadcrumbs::build($breadcrumbs) }}
    <div class="ud-mainpage">
      <table class="uk-table uk-table-hover uk-table-striped">
        <thead>
          <th class="uk-width-6-10">
            Forum
          </th>
          <th class="uk-width-2-10">
            Threads
          </th>
        </thead>
        <tbody>
          @if($forums) {{--If there are forums.--}}
            @foreach($forums as $forum)
              @if(Auth::user()->canViewForum($forum))
                <tr>
                  <td class="forum-name">
                    <a href="/community/forum/{{ $forum->id }}">
                      {{ $forum->name }}
                    </a>
                  </td>
                  <td>
                    {{ $forum->threads()->count() }}
                  </td>
                </tr>
              @endif
            @endforeach
            @else
            <tr class="nothreads">
              <td colspan="4">
                I'm afraid there are no threads for you *sadface*.
              </td>
            </tr>
            @endif
          </tbody>
        </table>
        {{--<a class="uk-button uk-button-large uk-button-primary uk-width-1-1" href="#">
          Load More
        </a>--}}
    </div>
  </div>
</div>
@stop
