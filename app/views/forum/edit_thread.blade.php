@extends('layout')
@section('content')
<div class="ud-container-pal4">
  <div class="ud-pagetitle uk-container uk-container-center">
    <h1 class="ud-pagetitle">
      Edit Thread
    </h1>
  </div>
</div>
<div class="ud-mainpage-bg uk-container uk-container-center">
  {{ breadcrumbs::build($breadcrumbs) }}
  <div class="uk-grid ud-mainpage uk-grid-divider" data-uk-grid-margin>
    <div class="uk-width-1-1">
      <form action="#" class="uk-form" method="POST" action="{{ $thread->url() }}/edit">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset class="uk-form-horizontal">
          <legend>
            Editing {{ $thread->title }}
          </legend>
          {{ View::make('tmpl.fancy_editor')->with('post', $thread)->render() }}
        </fieldset>
        <fieldset class="uk-container">
          <div class="uk-form-controls uk-grid">
            <button class="uk-button-large uk-button uk-width-1-1 uk-width-medium-1-2 uk-width-large-3-4 uk-button-success" type="submit">
              Edit
            </button>
            <button class="uk-button-large uk-button uk-hidden-small uk-width-medium-1-2 uk-width-large-1-4 uk-button-danger" type="reset">
              I Screwed Up
            </button>
          </div>
        </fieldset>
      </form>
    </div>
  </div>
</div>
@stop
