@extends('layout')
@section('content')
<div class="ud-container-pal4">
	<div class="ud-pagetitle uk-container uk-container-center">
		<h1 class="ud-pagetitle">
			{{ $thread->title }}
		</h1>
	</div>
</div>
<div class="ud-mainpage-bg uk-container uk-container-center">
	{{ Breadcrumbs::build($breadcrumbs) }}
	@if(Auth::user()->canLockThread($thread) || Auth::user()->canDeleteThread($thread))
	<div style="height:30px;">
		<div class="ud-clearfix">
			@if(Auth::user()->canLockThread($thread))
			<a class="uk-button uk-button-primary uk-float-right" href="/community/forum/{{ $thread->forum->id }}/thread/{{ $thread->id }}/lock">
				Lock Thread
			</a>
			@endif
			@if(Auth::user()->canDeleteThread($thread))
			<a class="uk-button uk-button-danger uk-float-right" href="/community/forum/{{ $thread->forum->id }}/thread/{{ $thread->id }}/delete">
				Delete Thread
			</a>
			@endif
		</div>
	</div>
	@endif
	<div class="ud-post {{ $thread->user->getRankPostColors() }}" id="thread_post"> {{--May need class for rank of poster--}}
		<div class="ud-mainpage">
			<table>
				<tr>
					<td class="ud-post-info uk-hidden-small">
						<div class="uk-text-center">
							<a href="{{ $thread->user()->first()->profileUrl() }}">
								<img class="ud-avatar-100px" src="{{ $thread->user()->first()->gravatarUrl(100) }}">
							</a>
						</div>
						<div class="uk-text-center">
							<a href="{{ $thread->user()->first()->profileUrl() }}">
								{{ User::find($thread->user->id)->getRankIcons() }}
							</a>
							<a href="{{ $thread->user()->first()->profileUrl() }}">
								{{ ucfirst($thread->user->username) }}
							</a>
						</div>
					</td>
					<td class="ud-post-content">
						{{ $markdown->text($thread->content) }}
					</td>
				</tr>
			</table>
		</div>
		<div class="uk-clearfix ud-post-bar">
			<div class="ud-post-bar-left uk-float-left">
				<a class="ud-post-bar-link" href="#thread_post">
					<i class="uk-icon-link"></i>
				</a>
				By {{ ucfirst($thread->user->username) }} {{ $thread->created_at->diffForHumans() }}.
			</div>
			<div class="ud-post-bar-right uk-float-right">
				@if(Auth::user()->canPostInThread($thread))
				<a class="uk-button uk-button-success" href="{{ $thread->url() }}/post/create">
					Reply
				</a>
				@endif
				@if(Auth::user()->canEditThread($thread))
				<a class="uk-button" href="{{ $thread->url() }}/edit">
					Edit Post
				</a>
				@endif
			</div>
		</div>
	</div>
	@foreach($posts as $post)
		<div class="ud-post {{ $post->user->getRankPostColors() }}" id="post_{{$post->id}}"> {{--May need class for rank of poster--}}
			<div class="ud-mainpage">
				<table>
					<tr>
						<td class="ud-post-info uk-hidden-small">
							<div class="uk-text-center">
								<a href="{{ $post->user()->first()->profileUrl() }}">
									<img class="ud-avatar-100px" src="{{ $post->user()->first()->gravatarUrl(100) }}">
								</a>
							</div>
							<div class="uk-text-center">
								<a href="{{ $post->user()->first()->profileUrl() }}">
									{{ User::find($post->user->id)->getRankIcons() }}
								</a>
								<a href="{{ $post->user()->first()->profileUrl() }}">
									{{ ucfirst($post->user->username) }}
								</a>
							</div>
						</td>
						<td class="ud-post-content">
							{{ $markdown->text($post->content) }}
						</td>
					</tr>
				</table>
			</div>
			<div class="uk-clearfix ud-post-bar">
				<div class="ud-post-bar-left uk-float-left">
					<a class="ud-post-bar-link" href="#post_{{ $post->id }}">
						<i class="uk-icon-link"></i>
					</a>
					By {{ ucfirst($post->user->username) }} {{ $post->created_at->diffForHumans() }}.
				</div>
				<div class="ud-post-bar-right uk-float-right">
					@if(Auth::user()->canPostInThread($thread))
					<a class="uk-button uk-button-success" href="{{ $thread->url() }}/post/create">
						Reply
					</a>
					@endif
					@if(Auth::user()->canEditPost($post))
					<a class="uk-button" href="{{ $thread->url() }}/post/{{ $post->id }}/edit">
						Edit Post
					</a>
					@endif
					@if(Auth::user()->canDeletePost($post))
					<a class="uk-button uk-button-danger" href="{{ $thread->url() }}/post/{{ $post->id }}/delete">
						Delete
					</a>
					@endif
				</div>
			</div>
		</div>
	@endforeach
	{{--<a class="uk-button uk-button-large uk-button-primary uk-width-1-1 ud-load-more-spacing" href="#">
		Load More
	</a>--}}
</div>
@stop
