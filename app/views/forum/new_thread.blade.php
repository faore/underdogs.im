@extends('layout')
@section('content')
<div class="ud-container-pal4">
  <div class="ud-pagetitle uk-container uk-container-center">
    <h1 class="ud-pagetitle">
      New Thread
    </h1>
  </div>
</div>
<div class="ud-mainpage-bg uk-container uk-container-center">
  {{ breadcrumbs::build($breadcrumbs) }}
  <div class="uk-grid ud-mainpage uk-grid-divider" data-uk-grid-margin>
    <div class="uk-width-1-1">
      <form action="#" class="uk-form" method="POST" action="/community/forum/{{ $forum->forum_id }}/thread/create">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset class="uk-form-horizontal">
          <legend>
            Creating a New Thread in {{ $forum->name }}
          </legend>
            <label class="uk-form-label" for="thread_title">
              Thread Title
            </label>
            <div class="uk-form-controls">
              <input class="uk-width-1-1" name="thread_title" placeholder="What's this thread about?" type="text"></input>
            </div>
          {{ View::make('tmpl.fancy_editor')->render() }}
        </fieldset>
        <fieldset class="uk-container">
          <div class="uk-form-controls uk-grid">
            <button class="uk-button-large uk-button uk-width-1-1 uk-width-medium-1-2 uk-width-large-3-4 uk-button-success" type="submit">
              Create
            </button>
            <button class="uk-button-large uk-button uk-hidden-small uk-width-medium-1-2 uk-width-large-1-4 uk-button-danger" type="reset">
              I Screwed Up
            </button>
          </div>
        </fieldset>
      </form>
    </div>
  </div>
</div>
@stop
