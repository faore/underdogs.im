@extends('layout')
@section('content')
<div class="ud-container-pal4">
  <div class="ud-pagetitle uk-container uk-container-center">
    <h1 class="ud-pagetitle">
      {{ $forum->name }}
    </h1>
  </div>
</div>
<div class="ud-mainpage-bg uk-container uk-container-center">
  {{ Breadcrumbs::build($breadcrumbs) }}
  <div class="ud-mainpage">
    @if(Auth::user()->canPostInForum($forum))
    <div class="ud-clearfix">
      <a class="uk-button uk-button-primary uk-float-right" href="/community/forum/{{ $forum->id }}/thread/create">
        New Thread
      </a>
    </div>
    @endif
    <table class="uk-table uk-table-hover uk-table-striped">
      <thead>
        <th class="uk-width-6-10">
          Thread
        </th>
        <th class="uk-width-2-10">
          Last Poster
        </th>
        <th class="uk-width-1-10 uk-hidden-small">
          Views
        </th>
        <th class="uk-width-1-10">
          Replies
        </th>
      </thead>
      <tbody>
        @if($threads) {{--If there are threads.--}}
          @foreach($threads as $thread)
          	@if(User::find($thread->user_id)->hasRole('Watchdog') || User::find($thread->user_id)->hasRole('Topdog')) {{--If poster is officer--}}
              <tr class="officer-thread thread">
            @else
              <tr class="thread">
            @endif
              <td>
                <a href="{{ $thread->url() }}">
                  {{ $thread->title }}
                </a>
                <span class="uk-comment-meta ud-rpost-meta">
                  by {{ ucfirst(User::find($thread->user_id)->username) }}
                </span>
              </td>
              <td>
                @if($posts = $thread->posts()->first())
                <a href="{{ $thread->posts()->first()->user()->first()->profileUrl() }}">
                  <img class="ud-avatar-25px" src="{{ $thread->posts()->getQuery()->orderBy('created_at', 'desc')->first()->user()->first()->gravatarUrl() }}"/>
                  {{ $thread->posts()->getQuery()->orderBy('created_at', 'desc')->first()->user()->first()->username }}
                </a>
                @else
                <a href="{{ $thread->user()->first()->profileUrl() }}">
                  <img class="ud-avatar-25px" src="{{ $thread->user()->first()->gravatarUrl(25) }}"/>
                  {{ ucfirst($thread->user()->first()['username']) }}
                </a>
                @endif
              </td>
              <td>
                {{ $thread->views }}
              </td>
              <td class="uk-hidden-small">
                {{ $thread->posts()->count() }}
              </td>
            </tr>
          @endforeach
        @else
          <tr class="nothreads">
            <td colspan="4">
              I'm afraid there are no threads for you *sadface*.
            </td>
          </tr>
        @endif
      </tbody>
    </table>
    {{--<a class="uk-button uk-button-large uk-button-primary uk-width-1-1" href="#">
      Load More
    </a>--}}
  </div>
</div>
@stop
