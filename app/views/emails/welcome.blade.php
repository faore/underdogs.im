<p>Hello! Glad to see you're interested in becoming an Underdog!
Click <a href="https://www.underdogs.im/users/confirm/{{$user->id}}/{{$activationlink}}/">here</a> to activate your account!</p>
<p>If you can't click the link, copy this into your browser:
<p>https://www.underdogs.im/users/confirm/{{$user->id}}/{{urlencode($activationlink)}}/</p>
<p>We've already created your application, we'll have a look over it soon. You'll recieve an email when your account is approved/rejected.</p>
