<!doctype html>
	<head>
		<link href='http://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
		<style type="text/css" media="screen">
			html, body, #wrapper {
				height:100%;
				width: 100%;
				margin: 0;
				padding: 0;
				border: 0;
				background: #000 url(https://www.underdogs.im/charity-support/images/header.png);
				color: #fff;
				font-family: 'Oxygen', sans-serif;
				font-size: 10px;
			}
			#wrapper td {
				vertical-align: middle;
				text-align: center;
			}
			h1 {
				font-family: 'Oxygen', sans-serif;
				font-size: 50px;
			}
		</style>
	</head>
	<body>
		<table id="wrapper">
			<tr>
			<td>
				<!--<img src="/img/logos/logo.png" alt="" />-->
				<h1>Something Awesome is Coming</h1>
				<p>Now if only this maintenance page would go away.</p>
			</td>
		</tr>
   </table>
	</body>
</html>
