<!DOCTYPE html class="uk-height-1-1">
  <head>
    <title>
      Underdogs Guild
    </title>
    <meta charset="utf-8"/>
    <!--Mobile Device Support-->
    <meta content="width=device-width,user-scalable=no" name="viewport"/>
    <!--End Mobile Device Support-->
    <!--Stylesheets-->
    <link href="/css/ud_master.css" rel="stylesheet" type="text/css">
    {{--# %link{'rel' => 'stylesheet', :href => '/css/nojs.css', :type => 'text/css'}--}}
    <!--End Stylesheets-->
    <script src="/js/all.min.js"></script>
  </head>
  <body class="uk-height-1-1 ud-error">
      <noscript>
        <div class="nojs">
          <div>
            <h1>
              Oh no! You murdered the JavaScript!
            </h1>
            <p>
              Hi there person! You seem to have disabled JavaScript. While that is perfectly understandable, we designed this entire site using JavaScript to enhance it and make development easier. Unfortunately, you can't browse the site without it. It just won't work. So enable your JS and reload the page! It'll be well worth it. No viruses; no ads; just our website.
            </p>
          </div>
        </div>
      </noscript>
      @yield('content')
  </body>
</html>