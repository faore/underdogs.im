<div class="uk-form-row">
  @if(!isset($post))
    {{ Form::textarea('post_content', null, array('data-uk-markdownarea' => "{toolbar:['bold','italic','strike','link','picture','blockquote','listUl','listOl']}")) }}
  @else
    {{ Form::textarea('post_content', $post->content, array('data-uk-markdownarea' => "{toolbar:['bold','italic','strike','link','picture','blockquote','listUl','listOl']}")) }}
  @endif
</div>