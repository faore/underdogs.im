<!--Global Off Canvas-->
<div class="uk-offcanvas" id="status-offcanvas">
  <div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
    <!--JSON Search Result Retrieval. Not working yet.-->
    <form class="uk-search" data-uk-search="{source:'/searchresults.json'}">
      <input class="uk-search-field" placeholder="Search..." type="search"></input>
      <button class="uk-search-close" type="reset"></button>
    </form>
    <!--Off Canvas Statuses-->
    <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="<%= true %>">
      <li class="uk-nav-header">
        Server Statuses
      </li>
      <li>
        <a href="#">
          TeamSpeak
          <div class="uk-badge uk-badge-success">
            Online
          </div>
        </a>
      </li>
      <li>
        <a href="#">
          Starbound
          <div class="uk-badge uk-badge-success">
            Online
          </div>
        </a>
      </li>
      <li>
        <a href="#">
          Killing Floor
          <div class="uk-badge uk-badge-danger">
            Offline
          </div>
        </a>
      </li>
      <li>
        <a href="#">
          Minecraft
          <div class="uk-badge uk-badge-danger">
            Offline
          </div>
        </a>
      </li>
      <li>
        <a href="#">
          VPN
          <div class="uk-badge uk-badge-success">
            Online
          </div>
        </a>
      </li>
      <li class="uk-nav-header">
        Account
      </li>
      <li>
        <a href="#">
          Change Your Account
        </a>
      </li>
      <li>
        <a href="#">
          Change Your Profile
        </a>
      </li>
      <li>
        <a href="#">
          Private Messages
        </a>
      </li>
      <li class="uk-nav-header">
        Underdogs Management
      </li>
      <li class="uk-parent">
        <a href="#">
          Moderate
        </a>
        <ul class="uk-nav-sub">
          <li>
            <a href="#">
              Manage Bans
            </a>
          </li>
          <li>
            <a href="#">
              Manage Warnings
            </a>
          </li>
          <li>
            <a href="#">
              Edit User Profiles
            </a>
          </li>
          <li>
            <a href="#">
              Mass Delete Threads
            </a>
          </li>
          <li>
            <a href="#">
              Mass Archive Threads
            </a>
          </li>
        </ul>
      </li>
      <li class="uk-parent">
        <a href="#">
          Content
        </a>
        <ul class="uk-nav-sub">
          <li>
            <a href="#">
              Manage Pages
            </a>
          </li>
          <li>
            <a href="#">
              Homepage Slideshow
            </a>
          </li>
          <li>
            <a href="#">
              Manage Events
            </a>
          </li>
        </ul>
      </li>
      <li class="uk-parent">
        <a href="#">
          Wiki
        </a>
        <ul class="uk-nav-sub">
          <li>
            <a href="#">
              Edit Wiki Home
            </a>
          </li>
          <li>
            <a href="#">
              Disable/Enable Wiki
            </a>
          </li>
          <li>
            <a href="#">
              Mass Move/Delete Page
            </a>
          </li>
        </ul>
      </li>
      <li class="uk-parent">
        <a href="#">
          Administrate
        </a>
        <ul class="uk-nav-sub">
          <li>
            <a href="#">
              General Settings
            </a>
          </li>
          <li>
            <a href="#">
              Social Icons
            </a>
          </li>
          <li>
            <a href="#">
              Legal Pages
            </a>
          </li>
          <li>
            <a href="#">
              Guild Rules
            </a>
          </li>
          <li>
            <a href="#">
              Disable Website
            </a>
          </li>
          <li>
            <a href="#">
              Mass Archive/Delete Pages
            </a>
          </li>
        </ul>
      </li>
      <li class="uk-parent">
        <a href="#">
          Server
        </a>
        <ul class="uk-nav-sub">
          <li>
            <a href="#">
              Environment & Load Average
            </a>
          </li>
          <li>
            <a href="#">
              Analytics
            </a>
          </li>
          <li>
            <a href="#">
              Database
            </a>
          </li>
          <li>
            <a href="#">
              Security
            </a>
          </li>
          <li>
            <a href="#">
              Email Settings
            </a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</div>
<!--End Global Off Canvas-->