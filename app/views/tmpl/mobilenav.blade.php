<!--Global Off Canvas-->
<div class="uk-offcanvas" id="mobile-offcanvas">
  <div class="uk-offcanvas-bar">
    <!--Off Canvas Statuses
    -->
    <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav>
      {{--<li class="uk-nav-header">
        News & Events
      </li>
      <li>
        <a href="#">
          News
        </a>
      </li>
      <li class="uk-parent">
        <a href="#">
          Events
        </a>
        <ul class="uk-nav-sub">
          <li>
            <a href="#">
              Special Events
            </a>
          </li>
          <li>
            <a href="#">
              Steam Events
            </a>
          </li>
        </ul>
      </li>--}}
      <li class="uk-nav-header">
        Community
      </li>
      <li class="uk-parent">
        <a href="#">
          Inner Community
        </a>
        <ul class="uk-nav-sub">
          <li>
            <a href="/community/forum">
              Forum
            </a>
          </li>
          {{--<li>
            <a href="#">
              Members
            </a>
          </li>--}}
        </ul>
      </li>
      <li class="uk-nav-header">
        Servers
      </li>
      <li class="uk-parent">
        <a href="#">
          Utilities & VoIP
        </a>
        <ul class="uk-nav-sub">
          <li>
            <a href="#">
              TeamSpeak
            </a>
          </li>
        </ul>
      </li>
      {{--<li class="uk-parent">
        <a href="#">
          Game Servers
        </a>
        <ul class="uk-nav-sub">
          <li>
            <a href="#">
              Starbound
            </a>
          </li>
          <li>
            <a href="#">
              Minecraft
            </a>
          </li>
          <li>
            <a href="#">
              Killing Floor
            </a>
          </li>
        </ul>
      </li>
      <li class="uk-nav-header">
        Wiki
      </li>
      <li class="uk-parent">
        <a href="#">
          Sections
        </a>
        <ul class="uk-nav-sub">
          <li>
            <a href="#">
              Event Plans
            </a>
          </li>
          <li>
            <a href="#">
              Raid Information
            </a>
          </li>
          <li>
            <a href="#">
              Roster Information
            </a>
          </li>
        </ul>
      </li>
      <li class="uk-parent">
        <a href="#">
          Special
        </a>
        <ul class="uk-nav-sub">
          <li>
            <a href="#">
              Recent Edits
            </a>
          </li>
          <li>
            <a href="#">
              Create a New Page
            </a>
          </li>
        </ul>
      </li>--}}
    </ul>
  </div>
</div>
<!--End Global Off Canvas-->