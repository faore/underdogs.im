<!-- Needed on every page.-->
<script src="/js/jquery.js"></script>
<script src="/js/uikit.js"></script>
<!-- Needed on pages with a slideshow.-->
<script src="/js/jquery.cycle2.js"></script>
<!-- Needed only on pages that have a text area needing markdown parsing.-->
<script src="/js/codemirror.js"></script>
<script src="/js/markdown.js"></script>
<script src="/js/overlay.js"></script>
<script src="/js/xml.js"></script>
<script src="/js/gfm.js"></script>
<script src="/js/marked.js"></script>
<script src="/js/markdownarea.js"></script>
<!-- UIKit Addons. Some may not be needed. Will remove as necissary-->
<script src="/js/form-file.js"></script>
<script src="/js/form-password.js"></script>
<script src="/js/notify.js"></script>
<script src="/js/overlay.js"></script>
<script src="/js/sortable.js"></script>
<script src="/js/sticky.js"></script>
<script src="/js/timepicker.js"></script>
<!-- Secure Password Hashing-->
<script src="/js/sha512.js"></script>
<!-- Feedback Lightbox-->
<script src="/js/jquery.colorbox.js"></script>
<!-- FFXIVDB Database content tooltips-->
<script src="http://xivdb.com/tooltips.js"></script>
<script>
	var xivdb_tooltips =
	{
		'language' : 'EN',
		'convertQuotes' : true,
		'frameShadow' : false,
		'compact' : false,
		'statsOnly' : false,
		'replaceName' : true,
		'colorName' : true,
		'showIcon' : true,
	}
</script>