<!--SEO and Social Media Metadata-->
<!--Google-->
<meta content="Official website for a casual FFXIV guild on Leviathan. We're recruiting! Join us now!" name="description"/>
<meta href="https://plus.google.com/113839180459420304737" rel="publisher"/>
<!--Facebook Data-->
<meta content="Underdogs Guild" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="http://cdnc.underdogs.im/img/meta/og-img.png" property="og:image"/>
<meta content="http://www.underdogs.im" property="og:url"/>
<meta content="Why sit here and read Facebook when you can game with us? Check out the Underdogs Guild!" property="og:description"/>
<meta content="100000385211055" property="fb:admins"/>
<!--Twitter Data-->
<meta content="summary" name="twitter:card"/>
<meta content="http://www.underdogs.im" name="twitter:url"/>
<meta content="Underdogs Guild" name="twitter:title"/>
<meta content="Why sit here and scroll through Twitter when you can game with us? Check out the Underdogs Guild!" name="twitter:description"/>
<meta content="http://www.underdogs.im/img/meta/tw-img.png" name="twitter:image"/>
<meta content="Copyright Underdogs Guild {{ date('Y') }}. All Rights Reserved" name="Copyright"/>
<!--End SEO and Social Media Metadata-->
<script type="text/javascript">
    var _bftn_options = { animation: 'banner' }
</script>
