<!--Navbar-->
<nav class="uk-navbar uk-navbar-attached" data-uk-sticky>
  <a class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas href="#mobile-offcanvas"></a>
  <a class="uk-navbar-brand uk-visible-small" href="/">
    <img alt="Underdogs Logo" class="ud-navbar-logo" src="/img/logos/logo.png"/>
  </a>
  <ul class="uk-navbar-nav uk-hidden-small">
    <!--Active Element-->
    <li class="uk-active">
      <a class="uk-navbar-nav-subtitle" href="/">
        Home
        <div>
          Once an Underdog...
        </div>
      </a>
    </li>
    {{--<li class="uk-parent" data-uk-dropdown>
      <a class="uk-navbar-nav-subtitle" href="#">
        News &amp; Events
        <div>
          Do Stuff!
        </div>
      </a>
      <div class="uk-dropdown uk-dropdown-navbar">
        <ul class="uk-nav uk-nav-navbar">
          <li>
            <a href="#">
              News
            </a>
          </li>
          <li class="uk-nav-header">
            Events
          </li>
          <li>
            <a href="#">
              Special Events
            </a>
          </li>
          <li>
            <a href="#">
              Stream Events
            </a>
          </li>
        </ul>
      </div>
    </li>--}}
    <li class="uk-parent" data-uk-dropdown>
      <a class="uk-navbar-nav-subtitle" href="/community/forum">
        Community
        <div>
          Get Together
        </div>
      </a>
      <div class="uk-dropdown uk-dropdown-navbar">
        <ul class="uk-nav uk-nav-navbar">
          <li class="uk-nav-header">
              Inner Community
          </li>
          <li>
            <a href="/community/forum">
              Forum
            </a>
          </li>
          {{--<li>
            <a>
              Members
            </a>
          </li>--}}
        </ul>
      </div>
    </li>
    <li class="uk-parent" data-uk-dropdown>
      <a class="uk-navbar-nav-subtitle" href="#">
        Servers
        <div>
          VoIP &amp; Games
        </div>
      </a>
      <div class="uk-dropdown uk-dropdown-navbar">
        <ul class="uk-nav uk-nav-navbar">
          <li class="uk-nav-header">
            Utilities and VoIP
          </li>
          <li>
            <a href="/servers/teamspeak">
              TeamSpeak
            </a>
          </li>
          {{--<li class="uk-nav-header">
            Game Servers
          </li>
          <li>
            <a>
              Starbound
            </a>
          </li>
          <li>
            <a>
              Minecraft
            </a>
          </li>
          <li>
            <a>
              Killing Floor
            </a>
          </li>--}}
        </ul>
      </div>
    </li>
    <li>
	<a class="uk-navbar-nav-subtitle" href="/charity">
	    24-Hour Live Stream Event
	    <div>
		Giving to Those In Need
	    </div>
	</a>
    </li>
    {{--<li class="uk-parent" data-uk-dropdown>
      <a class="uk-navbar-nav-subtitle" href="#">
        Wiki
        <div>
          Planning
        </div>
      </a>
      <div class="uk-dropdown uk-dropdown-navbar">
        <ul class="uk-nav uk-nav-navbar">
          <li class="uk-nav-header">
            Sections
          </li>
          <li>
            <a href="#">
              Event Plans
            </a>
          </li>
          <li>
            <a href="#">
              Raid Information
            </a>
          </li>
          <li>
            <a href="#">
              Roster Information
            </a>
          </li>
          <li class="uk-nav-header">
            Special
          </li>
          <li>
            <a href="#">
              Recent Edits
            </a>
          </li>
          <li>
            <a href="#">
              Create a New Page
            </a>
          </li>
        </ul>
      </div>
    </li>--}}
  </ul>
  <div class="uk-navbar-flip">
    <ul class="uk-navbar-nav">
      @if(!Auth::check())
        <li class="uk-parent" data-uk-dropdown="{mode'click'}">
          <a href="#">
            Login
            <i class="uk-icon-caret-down"></i>
          </a>
          <div class="uk-dropdown uk-dropdown-width-1 uk-dropdown-navbar">
            <div class="uk-grid">
              <div class="uk-width-1-1">
                <form action="/users/login" class="uk-form" id="navlogin" method="POST">
                  <fieldset>
                    <legend>
                      Sign In
                    </legend>
                    <div class="uk-form-row">
                      <input name="username" placeholder="UnderdogsID" type="text">
                    </div>
                    <div class="uk-form-row">
                      <input name="password" placeholder="Password" type="password">
                    </div>
                    <div class="uk-form-row">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input name="remember" type="checkbox">
                      Remember Me
                    </div>
                    <div class="uk-form-row">
                      <button class="uk-button" name="action" type="submit" value="login">
                        Login!
                      </button>
                      <a class="uk-button" href="/users/create">
                        Join Us!
                      </a>
                    </div>
                    <div class="uk-form-row">
                      <a href="#">
                        Forget your password?
                      </a>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </li>
      @else
        <li class="user-active uk-parent" data-uk-dropdown="{mode'click'}">
          <a href="#">
            {{ Extras::random_salutation(Auth::user()->username) }}
            <i class="uk-icon-caret-down"></i>
          </a>
          <div class="uk-dropdown uk-dropdown-width-2 uk-dropdown-navbar">
            <div class="uk-grid">
              <div class="uk-width-1-2">
                <ul class="uk-nav">
                  <li>
                    <a href="#">
                      <i class="uk-icon uk-icon-key"></i>
                      Account Settings
                    </a>
                  </li>
                </ul>
                  <small>Account Settings and donations are not available in Beta 1. If you need to change a password, email or username get in touch with an officer.</small>
              </div>
              <div class="uk-width-1-2">
                <div>
                  <a class="uk-button uk-button-danger uk-width-1-1 fb-open" href="#feedback-form">
                    <i class="uk-icon uk-icon-bug"></i>
                    Report a Bug
                  </a>
                </div>
                <div>
                  <a class="uk-button uk-button-success uk-width-1-1" href="#">
                    <i class="uk-icon uk-icon-money"></i>
                    Donate
                  </a>
                </div>
                <div>
                  <a class="uk-button uk-button-primary uk-width-1-1" href="/users/logout">
                    <i class="uk-icon uk-icon-sign-out"></i>
                    Logout
                  </a>
                </div>
              </div>
            </div>
          </div>
        </li>
      @endif
      {{--<li>
        <a data-uk-offcanvas href="#status-offcanvas">
          <i class="uk-icon-cog uk-icon-medium"></i>
        </a>
      </li>--}}
    </ul>
  </div>
</nav>
<!--End Navbar-->
