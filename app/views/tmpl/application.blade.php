## System Generated Application

Applicant Information:
* Username: {{Input::get('username')}}
* Character Name: {{Input::get('character_name')}}
* Class: {{Input::get('class')}}
* Age: {{Input::get('age')}}
* Past Guild Experience: {{Input::get('past_guild_experience')}}
* Five Words: {{Input::get('five_words')}}
* Where did you hear of us?: {{Input::get('where_did_you_hear_of_us')}}
* Anything Else?: {{Input::get('anything_else')}}
