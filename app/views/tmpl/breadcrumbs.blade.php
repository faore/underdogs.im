<div class="ud-breadcrumb">
  <ul class="uk-breadcrumb">
    {{--<!--- Breadcrumb with a link-->
    <li>
      <a href="/">
        Home
      </a>
    </li>
    <!--- Breadcrumb that is disabled-->
    <li>
      <span>
        Community
      </span>
    </li>
    <!--- Current Breadcrumb-->
    <li class="uk-active">
      <span>
        Forum
      </span>
    </li>--}}
    {{ $breadcrumbs }}
  </ul>
</div>