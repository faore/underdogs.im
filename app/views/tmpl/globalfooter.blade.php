<!--Footer-->
<div class="ud-container-pal2">
  <div class="ud-footer-inner uk-container uk-container-center">
    <div class="ud-footersocial uk-container uk-container-center">
      <a class="uk-icon-button uk-icon-facebook" href="https://www.facebook.com/UnderdogsGaming"></a>
      <a class="uk-icon-button uk-icon-google-plus" href="https://plus.google.com/+UnderdogsImFC"></a>
      <a class="uk-icon-button uk-icon-youtube" href="https://www.youtube.com/user/TheUnderdogsGuild"></a>
      <a class="uk-icon-button uk-icon-twitter" href="https://twitter.com/UnderdogsGuild"></a>
    </div>
    {{--<div class="ud-footernav">
      <ul class="uk-subnav uk-subnav-line">
        <li>
          <a href="#">
            Terms of Use
          </a>
        </li>
        <li>
          <a href="#">
            Privacy
          </a>
        </li>
        <li>
          <a href="#">
            Guild Rules
          </a>
        </li>
      </ul>
    </div>--}}
    <div class="ud-footer-text">
      Designed and coded with love and tears by Faore for the Underdogs Guild.
      <br/>
      <i class="uk-icon-heart"></i>
    </div>
    <div class="ud-copyright">
      &copy; Copyright 2014 Underdogs Guild. UDS 1.0 Beta 1 Update 3 Revision 3.<br /><img src="https://magnum.travis-ci.com/faore/underdogs.im.svg?token=tzxNsytXzg4owajAcSDG&branch=master" />
    </div>
  </div>
</div>
<!--End Footer-->
