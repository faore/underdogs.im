@extends('layout')
@section('content')
<!--Main Content-->
<div class="ud-mainpage-bg uk-container uk-container-center">
  {{ Breadcrumbs::build($breadcrumbs) }}
  <div class="uk-grid uk-grid-divider ud-mainpage" data-uk-grid-margin="<%= true %>">
    <div class="uk-width-large-1-5 uk-visible-large">
      <h2>
        Active Threads
      </h2>
      @foreach(Thread::orderBy('updated_at', 'desc')->get()->take(5) as $thread)
      <div class="ud-rpost-cont">
        <h6 class="ud-rpost-title uk-text-truncate">
          <a href="{{ $thread->url() }}">{{ $thread->title }}</a>
        </h6>
        <p class="uk-comment-meta ud-rpost-meta">
          By {{ ucfirst($thread->user()->first()['username']) }}.
          @if($thread->posts()->first())
          Last post by {{ ucfirst(User::find($thread->posts()->getQuery()->orderBy('created_at', 'desc')->first()['user_id'])['username']) }}.
          @endif
        </p>
      </div>
      @endforeach
    </div>
    <div class="uk-width-large-2-5 uk-width-medium-1-2">
      <h2>
        Upcoming Events
      </h2>
      <article class="uk-article">
        <h3 class="uk-article-title">
          Not Available
        </h3>
        <p class="uk-article-meta">
          By
          <img alt="SA" class="ud-rank" data-uk-tooltip src="/img/rank-ico/sa-ico.png" title="Server Administrator"/>
          <img alt="WD" class="ud-rank" data-uk-tooltip src="/img/rank-ico/wd-ico.png" title="Watchdog"/>
          Faore.
        </p>
        <p>
          The event stream is not available during UDS Beta 1.
        </p>
      </article>
      {{--<article class="uk-article">
        <h3 class="uk-article-title">
          Derp Knights
        </h3>
        <p class="uk-article-meta">
          By
          <img alt="TD" class="ud-rank" data-uk-tooltip src="/img/rank-ico/td-ico.png" title="Top Dog"/>
          Mirakell on January 31, 2014.
        </p>
        <p>
          Neque porro quisquam est, qui dolorem ipsum quia
          <a href="http://xivdb.com/?item/1955/Artemis-Bow">
            dolor
          </a>
          sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
        </p>
        <a class="uk-button ud-readmore uk-buttonmini uk-button-primary" href="#">
          Read More
        </a>
      </article>
      <article class="uk-article">
        <h3 class="uk-article-title">
          Herp Nites
        </h3>
        <p class="uk-article-meta">
          By
          <img alt="TD" class="ud-rank" data-uk-tooltip src="/img/rank-ico/td-ico.png" title="Top Dog"/>
          Mirakell on January 31, 2014.
        </p>
        <p>
          Aenean pharetra a purus vitae dictum. Curabitur placerat pellentesque nisl in facilisis. Nam vehicula, sapien ut tristique elementum, sapien eros tristique nisi, a lobortis eros nibh ut libero. Praesent at ipsum ultrices, rutrum quam non, consequat sem. Mauris accumsan vitae felis eget scelerisque. Integer volutpat est eu commodo placerat. Proin gravida, metus et placerat interdum, turpis elit varius dui, quis tincidunt leo enim id eros. Etiam convallis pharetra velit at cursus. Aliquam consequat mauris non lacus scelerisque, quis fermentum enim condimentum. Cras fermentum est in tortor cursus cursus.
        </p>
        <a class="uk-button ud-readmore uk-buttonmini uk-button-primary" href="#">
          Read More
        </a>
      </article>--}}
    </div>
    <div class="uk-width-large-2-5 uk-width-medium-1-2">
      <h2>
        Recent News
      </h2>
      <article class="uk-article">
        <h3 class="uk-article-title">
          Not Available
        </h3>
        <p class="uk-article-meta">
          By
          <img alt="SA" class="ud-rank" data-uk-tooltip src="/img/rank-ico/sa-ico.png" title="Server Administrator"/>
          <img alt="WD" class="ud-rank" data-uk-tooltip src="/img/rank-ico/wd-ico.png" title="Watchdog"/>
          Faore.
        </p>
        <p>
          The news stream is not available during UDS Beta 1.
        </p>
      </article>
      {{--<article class="uk-article">
        <h3 class="uk-article-title">
          New Website Up and Running
        </h3>
        <p class="uk-article-meta">
          By
          <img alt="SA" class="ud-rank" data-uk-tooltip src="/img/rank-ico/sa-ico.png" title="Server Administrator"/>
          <img alt="WD" class="ud-rank" data-uk-tooltip src="/img/rank-ico/wd-ico.png" title="Watchdog"/>
          Faore on January 31, 2014.
        </p>
        <p>
          Rawr. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
        </p>
        <a class="uk-button ud-readmore uk-buttonmini uk-button-primary" href="#">
          Read More
        </a>
      </article>
      <article class="uk-article">
        <h3 class="uk-article-title">
          Enjin Blown Up
        </h3>
        <p class="uk-article-meta">
          By
          <img alt="SA" class="ud-rank" data-uk-tooltip src="/img/rank-ico/sa-ico.png" title="Server Administrator"/>
          <img alt="WD" class="ud-rank" data-uk-tooltip src="/img/rank-ico/wd-ico.png" title="Watchdog"/>
          Faore on January 31, 2014.
        </p>
        <p>
          Kaboom. Nunc felis quam, posuere nec elementum nec, tempor in elit. Praesent sed ultrices tortor. Sed eu felis consectetur, interdum lorem sit amet, pretium erat. Phasellus eu orci risus. Quisque quis porttitor mi. Aliquam pellentesque felis ut fermentum eleifend. Mauris pretium aliquet lorem, in consequat odio. Suspendisse id augue eu nisi malesuada tincidunt sit amet vitae risus. Cras blandit magna eros. Nulla lacinia dapibus dui, ac sollicitudin massa commodo a. Aenean tempor nulla eu suscipit faucibus.
        </p>
        <a class="uk-button ud-readmore uk-buttonmini uk-button-primary" href="#">
          Read More
        </a>
      </article>--}}
    </div>
  </div>
</div>
<!--%div.ud-transbutton-bg-->
<!--	%div.uk-container.uk-container-center.ud-transbutton-trans-->
<!--		%div.uk-container.ud-transbutton-content.uk-text-center-->
<!--			%h3.ud-transbutton-text-->
<!--				Once and Underdog, Always an Underdog!-->
<!--			%a.uk-button.uk-button-large.uk-button-success{:href => '#'}-->
<!--				Apply Now!-->
@stop
