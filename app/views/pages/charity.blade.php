<!DOCTYPE HTML>
<html>
	<head>
		<title>Underdogs Charity Event</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="/charity-support/js/jquery.min.js"></script>
		<script src="/charity-support/js/jquery.dropotron.min.js"></script>
		<script src="/charity-support/js/jquery.scrolly.min.js"></script>
		<script src="/charity-support/js/jquery.onvisible.min.js"></script>
		<script src="/charity-support/js/skel.min.js"></script>
		<script src="/charity-support/js/skel-layers.min.js"></script>
		<script src="/charity-support/js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="/charity-support/css/skel.css" />
			<link rel="stylesheet" href="/charity-support/css/style.css" />
			<link rel="stylesheet" href="/charity-support/css/style-desktop.css" />
			<link rel="stylesheet" href="/charity-support/css/style-noscript.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="/charity-support/css/ie/v8.css" /><![endif]-->
	</head>
	<body class="homepage">
		<!-- Header -->
			<div id="header">

				<!-- Inner -->
					<div class="inner">
						<header>
							<h1><a href="#" id="logo">Underdogs Charity Event</a></h1>
							<hr />
							<p>Raising ${{ CharityTracker::getGoalInfo()[1] }} for Unicef.</p>
						</header>
						<footer>
							<a href="#donate" class="button circled scrolly">Donate</a>
						</footer>
					</div>

				<!-- Nav -->
					<nav id="nav">
						<ul>
							<li><a href="/">Underdogs Home</a></li>
							<li><a href="#status" class="scrolly">Status</a></li>
							<li><a href="#how-it-works" class="scrolly">How It Works</a></li>
							<li><a href="#stream" class="scrolly">Live Stream</a></li>
							<li><a href="#donate" class="scrolly">Donate</a></li>
							<li><a href="#events" class="scrolly">Schedule</a></li>
						</ul>
					</nav>

			</div>

		<!-- Banner -->
			<section id="status" class="banner">
				<header>
					<h2>We're At ${{ round(CharityTracker::getGoalInfo()[0], 2) }} Out Of ${{ CharityTracker::getGoalInfo()[1] }}</h2>
					<p>
						Keep it up!
					</p>
				</header>
			</section>

			<section id="how-it-works" class="banner">
				<header>
					<h2>How It Works</h2>
					<p>
						Check Out What We're Doing and How To Get Involved.
					</p>
				</header>
			</section>
		<!-- Carousel -->
			<section class="carousel">
				<div class="reel">

					<article>
						<a href="#" class="image featured"><img src="/charity-support/images/pic01.jpg" alt="" /></a>
						<header>
							<h3><a href="#">24 Hour Stream</a></h3>
						</header>
						<p>Members of the Underdogs stream for 24 hours straight. Stream starts at 9AM PST on Black Friday.</p>
					</article>

					<article>
						<a href="#" class="image featured"><img src="/charity-support/images/pic02.jpg" alt="" /></a>
						<header>
							<h3><a href="#">Join In</a></h3>
						</header>
						<p>Join us on TeamSpeak, post comments, play games with us. You're welcome to do anything you want!</p>
					</article>

					<article>
						<a href="#" class="image featured"><img src="/charity-support/images/pic03.jpg" alt="" /></a>
						<header>
							<h3><a href="#">Donate</a></h3>
						</header>
						<p>If you like what we're doing, make a donation! All payments are managed by JustGiving and go to Unicef.</p>
					</article>

					<article>
						<a href="#" class="image featured"><img src="/charity-support/images/pic04.jpg" alt="" /></a>
						<header>
							<h3><a href="#">Spread the Word</a></h3>
						</header>
						<p>Let friends and family know of the event and see if they can help out as well!</p>
					</article>

					<article>
						<a href="#" class="image featured"><img src="/charity-support/images/pic05.jpg" alt="" /></a>
						<header>
							<h3><a href="#">Social Networks</a></h3>
						</header>
						<p>Let people on social networks like Facebook and Twitter know of th event. A simple post goes a long way!</p>
					</article>
				</div>
			</section>

		<!-- Main -->
			<div class="wrapper style2">

				<article id="stream" class="container special">
					<div class="video-wrapper">
						<iframe width="1280" height="720" src="//www.youtube.com/embed/JsnFGdgRoe4" frameborder="0" allowfullscreen></iframe>
					</div>
					<header id="donate">
						<h2><a href="#">Donate!</a></h2>
						<p>
							Anyone is Welcome to Help Out
						</p>
					</header>
					<p>
						On the 28th of November, our Guild Underdogs shall be running a 24 hour live stream event in order to raise money for our chosen charity, Unicef. We'll be playing a selection of games together and hope that some of you will be able to join us! Any type of donation is very welcomed and goes towards a good cause. We hope to be able to make someone's life at least a little bit better.
					</p>
					<footer>
						<a href="https://www.justgiving.com/UnderdogsCharityStream" class="button">Donate</a>
					</footer>
				</article>

			</div>
			<section id="events" class="banner">
				<header>
					<h2>Stream Schedule</h2>
					<p>
						Check Out What We're Doing and When We're Doing It
					</p>
				</header>
			</section>
			<section class="carousel">
				<div class="reel">
					<article>
						<a href="" class="image featured"><img src="/charity-support/images/trine.jpg" alt="" /></a>
						<header>
							<h3><a href="">Trine</a></h3>
						</header>
						<p>Starting at 9AM PST Friday</p>
						<p>Faore, Mirakell, Tevin</p>
					</article>

          <article>
          	<a href="" class="image featured"><img src="/charity-support/images/hammerwatch.jpg" alt="" /></a>
            	<header>
                <h3><a href="">Hammerwatch</a></h3>
              </header>
              <p>Starting at 12PM PST Friday</p>
              <p>Faore, Mirakell, Tevin</p>
          </article>

					<article>
						<a href="" class="image featured"><img src="/charity-support/images/ffxiv.jpg" alt="" /></a>
						<header>
							<h3><a href="">Final Fantasy XIV</a></h3>
						</header>
						<p>Starting at 2PM PST Friday</p>
						<p>Faore, Mirakell, Tevin</p>
					</article>

          <article>
            <a href="" class="image featured"><img src="/charity-support/images/dungeondefenders.jpg" alt="" /></a>
            <header>
              <h3><a href="">Dungeon Defenders</a></h3>
            </header>
            <p>Starting at 5PM PST Friday</p>
            <p>Faore, Mirakell, Tevin</p>
          </article>

					<article>
						<a href="" class="image featured"><img src="/charity-support/images/trine.jpg" alt="" /></a>
						<header>
							<h3><a href="">Trine</a></h3>
						</header>
						<p>Starting at 8PM PST Friday</p>
						<p>Faore, Mirakell, Tevin</p>
					</article>

          <article>
            <a href="" class="image featured"><img src="/charity-support/images/torchlight2.jpg" alt="" /></a>
            <header>
            	<h3><a href="">Torchlight 2</a></h3>
            </header>
            <p>Starting at 11PM PST Friday</p>
            <p>Faore, Mirakell, Tevin</p>
          </article>

          <article>
            <a href="" class="image featured"><img src="/charity-support/images/dungeondefenders.jpg" alt="" /></a>
            <header>
              <h3><a href="">Dungeon Defenders</a></h3>
          	</header>
            <p>Starting at 12:30AM PST Saturday</p>
            <p>Faore, Mirakell, Tevin</p>
          </article>

          <article>
            <a href="" class="image featured"><img src="/charity-support/images/ffxiv.jpg" alt="" /></a>
            <header>
              <h3><a href="">Final Fantasy XIV</a></h3>
            </header>
            <p>Starting at 2AM PST Saturday</p>
            <p>Faore, Mirakell, Tevin</p>
          </article>

          <article>
            <a href="" class="image featured"><img src="/charity-support/images/dungeondefenders.jpg" alt="" /></a>
            <header>
              <h3><a href="">Dungeon Defenders</a></h3>
            </header>
            <p>Starting at 4:30AM PST Saturday</p>
          	<p>Faore, Mirakell, Tevin</p>
          </article>

          <article>
            <a href="" class="image featured"><img src="/charity-support/images/monaco.jpg" alt="" /></a>
            <header>
            	<h3><a href="">Monaco</a></h3>
            </header>
            <p>Starting at 6AM PST Saturday</p>
            <p>Faore, Mirakell, Tevin</p>
          </article>

				</div>
			</section>

		<!-- Footer -->
			<div id="footer">
				<div class="container">
					<div class="row">

						<!-- Tweets -->
							<section class="6u">
								<header>
									<h2 class="icon fa-twitter circled"><span class="label">Tweets</span></h2>
								</header>
								<div id="twitter">
            						<a class="twitter-timeline"  href="https://twitter.com/UnderdogsGuild" data-widget-id="517836287726739456" data-chrome="nofooter transparent noborders noheader" height="100%" width="100%">Tweets by @UnderdogsGuild</a>
            						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            					</div>
							</section>

						<!-- Posts -->
							<section class="6u">
								<header>
									<h2 class="icon fa-file circled"><span class="label">Posts</span></h2>
								</header>
								<ul class="divided">
                                                                        <li>
                                                                                <article class="post stub">
                                                                                        <header>
                                                                                                <h3><a href="#">Tenative Schedule</a></h3>
                                                                                        </header>
                                                                                        <span class="timestamp">October 9</span>
                                                                                </article>
                                                                        </li>
									<li>
										<article class="post stub">
											<header>
												<h3><a href="#">Charity Chosen</a></h3>
											</header>
											<span class="timestamp">October 2</span>
										</article>
									</li>
									<li>
										<article class="post stub">
											<header>
												<h3><a href="#">Event Planned</a></h3>
											</header>
											<span class="timestamp">October 1</span>
										</article>
									</li>
								</ul>
							</section>
						</div>
					<hr />
					<div class="row">
						<div class="12u">

							<!-- Contact -->
								<section class="contact">
									<header>
										<h3>Like Social Networks?</h3>
									</header>
									<p>We've got them.</p>
									<ul class="icons">
										<li><a href="http://www.twitter.com/UnderdogsGuild" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
										<li><a href="https://www.facebook.com/UnderdogsGaming" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
										<li><a href="https://www.youtube.com/user/TheUnderdogsGuild" class="icon fa-youtube"><span class="label">YouTube</span></a></li>
										<li><a href="https://plus.google.com/+UnderdogsImFC/posts" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
										<li><a href="http://steamcommunity.com/groups/underdogsgaming" class="icon fa-steam"><span class="label">Steam</span></a></li>
									</ul>
								</section>

							<!-- Copyright -->
								<div class="copyright">
									<ul class="menu">
										<li>&copy; Underdogs Guild. All rights reserved.</li>
									</ul>
								</div>

						</div>

					</div>
				</div>
			</div>
			<script>
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				ga('create', 'UA-46777577-2', 'auto');
				ga('send', 'pageview');

			</script>
	</body>
</html>
