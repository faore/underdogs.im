@extends('layout')
@section('content')
<div class="ud-container-pal4">
  <div class="ud-pagetitle uk-container uk-container-center">
    <h1 class="ud-pagetitle">
      TeamSpeak
    </h1>
  </div>
</div>
<div class="ud-mainpage-bg uk-container uk-container-center">
  {{ Breadcrumbs::build($breadcrumbs) }}
  <div class="uk-grid ud-mainpage uk-grid-divider" data-uk-grid-margin>
    <div class="uk-width-1-1">
  	  <p>The Underdogs have their own dedicated TeamSpeak server where we encourage members, and people that we play with to join us on voice chat! You can check out the server status on the left. Whether you're someone considering joining, or already a member, we encourage you to join us!</p>
	  <a class="uk-button uk-button-large uk-button-success" data-uk-modal href="#tsmodal">Connect!</a>
	  <div class="uk-modal" id="tsmodal">
	    <div class="uk-modal-dialog">
	      <a class="uk-modal-close uk-close"></a>
	      <div class="modal-content">
	        <div class="modal-header">
	  		<h4 class="modal-title">Got TeamSpeak?</h4>
	  	  </div>
	  	  <div class="modal-body">
	  	    <p>To auto-connect to our TeamSpeak server you need to have TeamSpeak installed, and you need to be using Windows. If you're all set click "I Already Have it", if you need TeamSpeak click "I Need TeamSpeak" to download it from TeamSpeak.com.</p>
	  	    <p>If you're running a Mac, Linux, or are trying to connect on your mobile device, you will likely need to connect manually to <b>ts.underdogs.im</b>.</p>
	  	  </div>
	  	  <div class="modal-footer">
	  	    <a type="button" class="uk-button uk-button-success" href="ts3server://ts.underdogs.im">I Already Have It</a>
	  	    <a class="uk-button uk-button-info" href="http://www.teamspeak.com/?page=downloads">I Need TeamSpeak</a>
	  	  </div>
	      </div>
	    </div>
      </div>
    </div>
  </div>
</div>
@stop
