<div class="ud-container-pal4">
  <div class="ud-pagetitle uk-container uk-container-center">
    <h1 class="ud-pagetitle">
      Mission Complete! You're Awesome.
    </h1>
  </div>
</div>
<div class="ud-mainpage-bg uk-container uk-container-center">
  {{ View::make('tmpl/breadcrumbs')->render }}
  <div class="uk-grid ud-mainpage uk-grid-divider" data-uk-grid-margin>
    <div class="uk-width-1-2">
      <p>
        Congratulations! You've successfully donated to keep the Underdogs server running for another month. We greatly appreciate your contribution. If you have any issues reguarding the transaction, feel free to contact Faore.
      </p>
    </div>
    <div class="uk-width-1-2">
      <h2>
        Refunds
      </h2>
      <p>
        If you hadn't meant to donate the amount you did, tell Faore immediately to have the transaction reversed. Issuing refunds for payments via Paypal costs the guild money that could have been used towards the server (due to PayPal's fees), please request them rarely.
      </p>
    </div>
  </div>
</div>
