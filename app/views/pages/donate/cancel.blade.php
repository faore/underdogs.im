<div class="ud-container-pal4">
  <div class="ud-pagetitle uk-container uk-container-center">
    <h1 class="ud-pagetitle">
      Aww. Donation Canceled.
    </h1>
  </div>
</div>
<div class="ud-mainpage-bg uk-container uk-container-center">
  {{ View::make('tmpl/breadcrumbs')->render() }}
  <div class="uk-grid ud-mainpage uk-grid-divider" data-uk-grid-margin="<%= true %>">
    <div class="uk-width-1-2">
      <p>
        You're all set. We've canceled your donation. You won't need to do anything else and you will not be charged.
      </p>
    </div>
    <div class="uk-width-1-2">
      <h2>
        Need Help
      </h2>
      <p>
        If you're having issues donating or have any questions, ask Faore. He'll be able to set you in the right direction.
      </p>
    </div>
  </div>
</div>
