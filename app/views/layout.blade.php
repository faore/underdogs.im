<!doctype html>
  <head>
    <title>
      Underdogs Guild
    </title>
    <meta charset="utf-8"/>
    {{ View::make('tmpl.globalmeta')->render() }}
    <!--Mobile Device Support-->
    <meta content="width=device-width, user-scalable=no" name="viewport"/>
    <!--%link{'rel' => 'shortcut icon', 'href' => '/img/meta/favicon.ico'}-->
    <!--%link{'rel' => 'apple-touch-icon', 'href' => '/img/meta/apple-touch-icon.png'}-->
    <!--End Mobile Device Support-->
    <link href="/css/ud_master.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="ud-header-container">
      <div id="masthead">
        <a href="https://www.underdogs.im/" id="tabzilla">Underdogs</a>
        <nav id="secondary-nav">
          <ul class="ud-nav-main-menu">
            @if(Auth::check())
            <li>
              Hello {{ ucfirst(Auth::User()->username) }}!
            </li>
            <li>
              <a href="#">Account (Soon!)</a>
            </li>
            <li>
              <a href="#">Messages (Soon!)</a>
            </li>
            @endif

            <li>
              @if(Auth::check())
                <a href="/users/logout">Logout</a>
              @else
                <a href="/users/login">Login</a>
              @endif
            </li>
          </ul>
        </nav>
      </div>
    </div>
    <noscript>
      <div class="nojs">
        <div>
          <h1>
            Oh no! You murdered the JavaScript!
          </h1>
          <p>
            Hi there person! You seem to have disabled JavaScript.
            While that is perfectly understandable, we designed this
            entire site using JavaScript to enhance it and make
            development easier. Unfortunately, you can't browse the
            site without it. It just won't work. So enable your JS and
            reload the page! It'll be well worth it. No viruses; no
            ads; just our website.
          </p>
          <p>
            If you can't enable JavaScript or when you do, the site starts
            looking weird, it may be time to get yourself a modern browser.
          </p>
        </div>
      </div>
    </noscript>

    <!--Main Page-->
    <div class="uk-grid">
      <!--Notice Section-->
      @foreach(Notice::where('active', '=', 1)->get() as $notice)
      <div class="uk-width-1-1">
	       <div class="uk-alert uk-text-center {{$notice->class}}" data-uk-alert><a href="" class="uk-alert-close uk-close"></a>{{$notice->content}}</div>
      </div>
      @endforeach
      @if(Session::has('success'))
        <div class="uk-width-1-1">
          @foreach(Session::get('success') as $success)
            <div class="uk-alert uk-text-center uk-alert-success" data-uk-alert>
              <a href="" class="uk-alert-close uk-close"></a>
              {{$success}}
            </div>
          @endforeach
        </div>
      @endif
      @if(Session::has('info'))
        <div class="uk-width-1-1">
          @foreach(Session::get('info') as $info)
            <div class="uk-alert uk-text-center uk-alert-info" data-uk-alert>
              <a href="" class="uk-alert-close uk-close"></a>
              {{$info}}
            </div>
          @endforeach
        </div>
      @endif
      @if(Session::has('warning'))
        <div class="uk-width-1-1">
          @foreach(Session::get('warning') as $warning)
        <div class="uk-alert uk-text-center uk-alert-warning" data-uk-alert>
          <a href="" class="uk-alert-close uk-close"></a>
          {{$warning}}
        </div>
        @endforeach
      </div>
      @endif
      @if(Session::has('error'))
        <div class="uk-width-1-1">
          @foreach(Session::get('error') as $error)
            <div class="uk-alert uk-text-center uk-alert-danger" data-uk-alert>
              <a href="" class="uk-alert-close uk-close"></a>
              {{$error}}
            </div>
          @endforeach
        </div>
      @endif
      <!--End Notice Section-->
    </div>
      {{-- View::make('tmpl.globalnav')->render() --}}
    </div>
    <div class="ud-container-pal1">
      <div class="ud-container-center uk-text-center">
        <a href="/">
          <svg id="sir" height="216" width="216" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g transform="scale(0.2)">
              <image x="0" y="0" height="1140" width="1040"  xlink:href="/api/logo/00BD72/uds.svg"/>
            </g>
          </svg>
        </a>
      </div>
    </div>
    <!--Slideshow-->
    @if(isset($slideshow))
    <div class="ud-container-pal1">
      <div class="ud-slideshow-inner">
        <div class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-pause-on-hover="true" data-cylce-timeout="10000">
          <div class="cycle-prev"></div>
          <div class="cycle-next"></div>
          <div id="progress"></div>
          {{ Slide::createSlides() }}
        </div>
      </div>
    </div>
    @endif
    @if(!isset($noshout))
    @if(Auth::check())
    <div class="ud-container-pal1">
      <div class="uk-container uk-container-center ud-slideshow-inner">
        <div id="shoutbox" class="uk-width-1-1">
          <div id="shoutbox-list"></div>
          <br />
          <form id="shoutbox-form" class="uk-form uk-grid" action="/api/shoutbox/create" method="post">
            <div class="uk-width-1-1">
              <input id="shoutbox-textbox" class="uk-width-1-1" type="text" name="message" placeholder="Enter some awesomeness and press enter..." />
                <span id="shoutbox-response"></span>
            </div>
          </form>
        </div>
      </div>
    </div>
    @endif
    @endif
    <!--End Slideshow-->
      @yield('content')
    {{ View::make('tmpl.globalfooter')->render() }}
    <!--Feedback and Bug Reports-->
    @if(Auth::check())
    <div class="fb-hide">
      <div id="feedback-form">
        <div id="feedback-content">
          <form action="/feedback" class="uk-form" method="POST">
            <fieldset class="uk-form-horizontal">
              <legend>
                Give Us Feedback About This Page!
              </legend>
              <div class="uk-form-row">
                <label class="uk-form-label" for="username">
                  UnderdogsID
                </label>
                <div class="uk-form-controls">
                  @if(Auth::check())
                  <input class="uk-width-1-1" disabled name="username" type="text" value="{{ Auth::user()->username }}">
                  @else
                  <input class="uk-width-1-1" disabled name="username" type="text" value="Guest">
                  @endif
                </div>
              </div>
              <div class="uk-form-row">
                <label class="uk-form-label" for="location">
                  Location
                </label>
                <div class="uk-form-controls">
                  <input class="uk-width-1-1" disabled name="location_display" type="text" value="{{ Request::url() }}">
                  <input class="uk-width-1-1" name="location" type="hidden" value="{{ Request::url() }}">
                </div>
              </div>
              <div class="uk-form-row">
                <label class="uk-form-label" for="category">
                  Feedback Type
                </label>
                <div class="uk-form-controls">
                  <select class="uk-width-1-1" name="category">
                    <option>
                      Feedback/Suggestion
                    </option>
                    <option>
                      Bug Report
                    </option>
                  </select>
                </div>
              </div>
              <div class="uk-form-row">
                <label class="uk-form-label" for="feedback">
                  Feedback
                </label>
                <div class="uk-form-controls">
                  <textarea class="uk-width-1-1" name="feedback" placeholder="Not enough Tonberries!" style="resize:none;"></textarea>
                </div>
              </div>
              <div class="uk-form-row">
                <label class="uk-form-label">
                  All Done?
                </label>
                <div class="uk-form-controls">
                  <button class="uk-button uk-button-success" type="submit">
                    Send Feedback
                  </button>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
    @endif
    @if(Auth::check())
    <a class="uk-button uk-button-success fb-open" href="#feedback-form" style="position:fixed; bottom:0; right: 0;">
      <i class="uk-icon uk-icon-plus uk-icon-spin"></i>
    </a>
    @endif
    <script src="/js/all.min.js"></script>
    @if(!isset($noshout))
    @if(Auth::check())
    <script type="text/javascript">
        var count = 0;
        var files = '';
        var lastTime = 0;

        function prepare(response) {
          var d = new Date();
          count++;
          d.setTime(response.time*1000);
          var mytime = d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
          var string = '<div class="shoutbox-list" id="list-'+count+'">'
              + '<span class="shoutbox-list-time">'+mytime+'</span>'
              + '<span class="shoutbox-list-nick">'+response.nickname+':</span>'
              + '<span class="shoutbox-list-message">'+response.message+'</span>'
              +'</div>';

          return string;
        }

        function success(response, status)  {
          if(status == 'success') {
            console.log("Mission success. Updating shoutbox content.")
            lastTime = response.time;
            $('#shoutbox-response').html('<i class="uk-icon-check-circle uk-icon-medium"></i> ');
            $('#shoutbox-list').append(prepare(response));
            $('#shoutbox-textbox').val("");
            $('#list-'+count).fadeIn('slow');
            timeoutID = setTimeout(refresh, 3000);
          }
        }

        function error(response, status)  {
            $('#shoutbox-response').html('<i class="uk-icon-times-circle uk-icon-medium"></i> ');
            $('#shoutbox-textbox').focus();
            timeoutID = setTimeout(refresh, 3000);
        }

        function refresh() {
          $.getJSON(files+"/api/shoutbox/view?time="+lastTime, function(json) {
            if(json.length) {
              for(i=0; i < json.length; i++) {
                $('#shoutbox-list').append(prepare(json[i]));
                $('#list-' + count).fadeIn('slow');
              }
              var j = i-1;
              lastTime = json[j].time;
            }
            //alert(lastTime);
          });
          timeoutID = setTimeout(refresh, 3000);
        }

        // wait for the DOM to be loaded
        $(document).ready(function() {
            var options = {
              dataType:       'json',
              success:        success,
              error:          error
            };
            $('#shoutbox-form').ajaxForm(options);
            timeoutID = setTimeout(refresh, 100);
        });
    </script>
    @endif
    @endif
    <script src="/js/tabzilla.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-46777577-2', 'auto');
      ga('send', 'pageview');

    </script>
  </body>
</html>
