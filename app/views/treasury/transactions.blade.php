@extends('layout')
@section('content')
<div class="ud-container-pal4">
  <div class="ud-pagetitle uk-container uk-container-center">
    <h1 class="ud-pagetitle">
      Treasury
    </h1>
  </div>
</div>
<div class="ud-mainpage-bg uk-container uk-container-center">
  {{ Breadcrumbs::build($breadcrumbs) }}
  <div class="ud-mainpage">
    <div id="chart_div" style="width:100%; height: auto; min-height:500px;"></div>
    <table class="uk-table uk-table-hover uk-table-striped">
      <thead>
        <th>
          User
        </th>
        <th>
          Gil
        </th>
        <th>
          Transaction Code
        </th>
      </thead>
      <tbody>
        @if($transactions) {{--If there are transactions.--}}
          @foreach($transactions as $transaction)
              <tr>
                <td>
                  {{ ucfirst($transaction->user()->first()['username']) }}
                </td>
                <td>
                  {{ $transaction->gil }}
                </td>
                <td>
                  {{ $transaction->transactiontype->name }}
                </td>
              </tr>
          @endforeach
        @else
          <tr class="nothreads">
            <td colspan="3">
              The treasury is empty, sire. Go fix it!
            </td>
          </tr>
        @endif
      </tbody>
    </table>
    {{--<a class="uk-button uk-button-large uk-button-primary uk-width-1-1" href="#">
      Load More
    </a>--}}
  </div>
</div>
<script type="text/javascript" src="//www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      {{ $out }}
        ]);

        var options = {
          title: 'Donations Overtime',
          hAxis: {title: 'Day',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0},
          isStacked: true,
          backgroundColor: 'transparent'
        };

        function resize () {
          var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
          chart.draw(data, options);
        }
        window.onload = resize();
        window.onresize = resize;
      }
</script>
@stop
