<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ConvertPosts extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'ConvertPosts';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Converts posts from BBCode to Markdown.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function updatePost($postContent) {
		// Set up patterns and replacements
		$patterns = array();
		$replacements = array();

		// Bold text
		$patterns[] = '/\[b(old)?\](.+?)\[\/b(old)?\]/smi';
		$replacements[] = '**$2**';

		// Underlined text (actually emphasized)
		$patterns[] = '/\[u(nderline)?\](.+?)\[\/u(nderline)?\]/smi';
		$replacements[] = '*$2*';

		// Strikethrough
		$patterns[] = '/\[s(trike)?\](.+?)\[\/s(trike)?\]/smi';
		$replacements[] = '~~$2~~';

		// Images
		$patterns[] = '/\[img\](.+?)\[\/img\]/smi';
		$replacements[] = '![]($1)';

		// Color tags
		$patterns[] = '/\[color=.+?\](.+?)\[\/color\]/smi';
		$replacements[] = '$1';

		// Links
		$patterns[] = '/\[url=(.+?)\](.+?)\[\/url\]/smi';
		$replacements[] = '[$2]($1)';

		// Lists
		$patterns[] = '/\[list(=.+?)?\](.+?)\[\/list\]/smi';
		$replacements[] = '$2';

		$patterns[] = '/(\n)\[\*\]/smi';
		$replacements[] = '$1* ';

		// Center tags
		$patterns[] = '/\[center\](.+?)\[\/center\]/smi';
		$replacements[] = '$1';

		// Quotes
		$patterns[] = '/\[quote=.+?\](.+?)\[\/quote\]/smi';
		$replacements[] = '> $1';

		$patterns[] = '/> (.+?)\n(.+?)/smi';
		$replacements[] = "> $1\n> $2";

		// Attachments got lost, so remove their tags.
		$patterns[] = '/\[attachment=\d+?\](.+?)\[\/attachment\]/smi';
		$replacements[] = '';

		// Execute replacements
		return preg_replace($patterns, $replacements, $postContent);
	}
	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info('Searching all posts for BBcode.');
		foreach(Post::all() as $post) {
			$this->info('Updating Post ' . $post->id);
			$newContent = ConvertPosts::updatePost($post->content);
			$post->timestamps = false;
			$post->content = $newContent;
			$post->save();
		}
		foreach(Thread::all() as $thread) {
			$this->info('Updating Thread ' . $thread->id);
			$newContent = ConvertPosts::updatePost($thread->content);
			$thread->timestamps = false;
			$thread->content = $newContent;
			$thread->save();
		}
	}
}
