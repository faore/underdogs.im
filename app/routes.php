<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('pages.index')->with('breadcrumbs', Breadcrumbs::home())->with('slideshow', true);
});

Route::get('charity', function()
{
	return View::make('pages.charity');
});
//Treasury Routing
Route::get('treasury', 'TransactionsController@index')->before('auth');
//Forum Routing
Route::get('community/forum', 'ForumController@index')->before('auth');
Route::get('community/forum/{forum_id}', 'ForumController@show')->before('auth')->before('viewForum');
Route::get('community/forum/{forum_id}/thread/create', 'ThreadController@create')->before('auth')->before('postForum');
Route::post('community/forum/{forum_id}/thread/create', 'ThreadController@store')->before('auth')->before('postForum')->before('csrf');
Route::get('community/forum/{forum_id}/thread/{thread_id}', 'ThreadController@show')->before('auth')->before('viewForum');
Route::get('community/forum/{forum_id}/thread/{thread_id}/edit', 'ThreadController@edit')->before('auth')->before('editThread');
Route::post('community/forum/{forum_id}/thread/{thread_id}/edit', 'ThreadController@update')->before('auth')->before('editThread')->before('csrf');
Route::get('community/forum/{forum_id}/thread/{thread_id}/post/create', 'PostController@create')->before('auth')->before('postInThread');
Route::post('community/forum/{forum_id}/thread/{thread_id}/post/create', 'PostController@store')->before('auth')->before('postInThread')->before('csrf');
Route::get('community/forum/{forum_id}/thread/{thread_id}/post/{post_id}/edit', 'PostController@edit')->before('auth')->before('editPost');
Route::post('community/forum/{forum_id}/thread/{thread_id}/post/{post_id}/edit', 'PostController@update')->before('auth')->before('editPost')->before('csrf');

// UsersController
Route::get('users/create', 'UsersController@create')->before('guest'); //Registration Page
Route::post('users/create', 'UsersController@store')->before('guest'); //Registration Store
Route::get('users/login', 'UsersController@login')->before('guest'); //Login Page
Route::post('users/login', 'UsersController@doLogin')->before('guest'); //Login Process
Route::get('users/confirm/{id}/{code}', 'UsersController@confirm')->before('guest'); //Confirmation Email
Route::get('users/forgot_password', 'UsersController@forgotPassword')->before('guest'); //Forgot Password
Route::post('users/forgot_password', 'UsersController@doForgotPassword')->before('guest'); //Process Forgot Password
Route::get('users/reset_password/{token}', 'UsersController@resetPassword')->before('guest'); //Password Reset
Route::post('users/reset_password', 'UsersController@doResetPassword')->before('guest'); //Password Reset Process
Route::get('users/logout', 'UsersController@logout')->before('auth'); //Logout

//Feedback Form
Route::post('feedback', 'FeedbackController@store')->before('auth');

//Servers Menu Items
Route::get('servers/teamspeak', function() {
	return View::make('pages.teamspeak')->with('breadcrumbs', Breadcrumbs::teamspeak());
});

//API
Route::get('api/shoutbox/view', 'ApiController@shoutboxView')->before('auth'); //Shoutbox Ajax View
Route::post('api/shoutbox/create', 'ApiController@shoutboxStore')->before('auth'); //Create a shout.
Route::get('api/logo/{color}/uds.svg', 'ApiController@logoSvg');
